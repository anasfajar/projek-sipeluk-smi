<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key_kode');
            $table->string('kode_p');
            $table->string('kode_k')->nullable();
            $table->string('kode_t')->nullable();
            $table->string('kode_i')->nullable();
            $table->string('tipe_kode')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('aktivitas')->nullable();
            $table->string('program')->nullable();
            $table->date('tgl_mulai')->nullable();
            $table->date('tgl_selesai')->nullable();
            $table->integer('durasi')->nullable();
            $table->date('tgl_pelaksanaan')->nullable();
            $table->string('output')->nullable();
            $table->longText('keterangan')->nullable();
            $table->string('keterangan_durasi')->nullable();
            $table->integer('persentase')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_kegiatans');
    }
}
