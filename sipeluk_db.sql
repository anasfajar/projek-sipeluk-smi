-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for sipeluk_db
CREATE DATABASE IF NOT EXISTS `sipeluk_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sipeluk_db`;

-- Dumping structure for table sipeluk_db.master_kegiatan
CREATE TABLE IF NOT EXISTS `master_kegiatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key_kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_p` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_k` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_t` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_i` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aktivitas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `tgl_pelaksanaan` date DEFAULT NULL,
  `output` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan_durasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.master_kegiatan: ~5 rows (approximately)
/*!40000 ALTER TABLE `master_kegiatan` DISABLE KEYS */;
INSERT INTO `master_kegiatan` (`id`, `key_kode`, `kode_p`, `kode_k`, `kode_t`, `kode_i`, `tipe_kode`, `user_id`, `aktivitas`, `program`, `tgl_mulai`, `tgl_selesai`, `durasi`, `tgl_pelaksanaan`, `output`, `keterangan`, `keterangan_durasi`, `persentase`, `created_at`, `updated_at`) VALUES
	(29, '0', '22', NULL, NULL, NULL, '1', NULL, 'Perencanaan RKPD', NULL, '2019-10-21', '2019-10-26', 6, NULL, NULL, NULL, NULL, NULL, '2019-10-23 17:00:29', '2019-10-23 17:04:08'),
	(30, '22', '22', '200', NULL, '22.200', '2', '3', 'Pembukuan Pengeluaran RKPD', NULL, '2019-10-22', '2019-10-25', 4, NULL, NULL, NULL, NULL, NULL, '2019-10-23 17:01:13', '2019-10-23 17:04:08'),
	(31, '22', '22', '100', NULL, '22.100', '2', NULL, 'Pembuatan Buku Penutup', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-23 17:02:06', '2019-10-23 17:02:06'),
	(32, '22', '22', '200', '001', '22.200.001', '3', '3', 'Tahapan', NULL, '2019-10-23', '2019-10-24', 2, NULL, NULL, NULL, NULL, NULL, '2019-10-23 17:04:08', '2019-10-23 17:04:08'),
	(33, '22', '22', '200', '001', '22.200.001.001', '4', '3', '1. Pemberesan Pembukuan', NULL, '2019-10-23', '2019-10-24', 2, '2019-10-23', NULL, '2', 'Pelaksanaan/2019-10-23/Images/2019-10-23Images.png', 92, '2019-10-23 17:05:23', '2019-10-23 17:08:31');
/*!40000 ALTER TABLE `master_kegiatan` ENABLE KEYS */;

-- Dumping structure for table sipeluk_db.master_pegawai
CREATE TABLE IF NOT EXISTS `master_pegawai` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nip_pegawai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pegawai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_pegawai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_pegawai` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.master_pegawai: ~2 rows (approximately)
/*!40000 ALTER TABLE `master_pegawai` DISABLE KEYS */;
INSERT INTO `master_pegawai` (`id`, `id_user`, `nip_pegawai`, `nama_pegawai`, `jabatan_pegawai`, `keterangan_pegawai`, `created_at`, `updated_at`) VALUES
	(1, 2, '9293939', 'Kabid Satu', 'Kepala Bidang', 'wdwqdq', '2019-09-21 03:00:25', '2019-09-21 03:00:25'),
	(2, 3, '21983128301', 'Kasubid', 'Kepala Sub Bidang', 'wkwkw', '2019-09-27 17:43:19', '2019-09-27 17:43:19');
/*!40000 ALTER TABLE `master_pegawai` ENABLE KEYS */;

-- Dumping structure for table sipeluk_db.master_program
CREATE TABLE IF NOT EXISTS `master_program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_program` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.master_program: ~0 rows (approximately)
/*!40000 ALTER TABLE `master_program` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_program` ENABLE KEYS */;

-- Dumping structure for table sipeluk_db.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.migrations: ~5 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_13_145736_create_master_programs_table', 1),
	(4, '2019_08_13_175214_create_master_kegiatans_table', 1),
	(5, '2019_08_14_070227_create_master_pegawais_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table sipeluk_db.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table sipeluk_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sipeluk_db.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@mail.com', NULL, '$2y$10$sXtQlsmNXu2RLTzdRajnKeQBX9zweOjdZ8sY0LVw0A6Uo1gnvyHmm', 'admin', NULL, NULL, NULL),
	(2, 'Kabid', 'kabid@mail.com', NULL, '$2y$10$m0iYLNa/cZ8.GDrn1xTubOPrVTA.zCIREYUVNaiiVCD7de2KLUu7m', 'kabid', NULL, '2019-09-21 03:00:25', '2019-09-21 03:00:25'),
	(3, 'Kasubid 1', 'kasubid@mail.com', NULL, '$2y$10$eOvsOtTcNTFBG/I5k2IhCOEgaOrZB0D8u83tkCNRMRJFporqd4SKC', 'kasubid', NULL, '2019-09-27 17:43:19', '2019-09-28 08:20:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
