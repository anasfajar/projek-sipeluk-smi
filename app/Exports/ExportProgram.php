<?php

namespace App\Exports;

use App\MasterKegiatan;
use App\MasterPegawai;
use App\Users;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
Use \Maatwebsite\Excel\Sheet;

class ExportProgram implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $dataArr;
    public function __construct(string $kodeInisial)
    {
      $this->dataArr = $kodeInisial;
    }

    public function view(): View
    {
        $id = $this->dataArr;
        // $data = MasterKegiatan::where('kode_i',$id)->leftJoin('users','users.id','master_kegiatan.user_id')->leftJoin('master_pegawai','master_pegawai.id_user','users.id')->first(['master_pegawai.keterangan_pegawai','master_pegawai.nama_pegawai']);
        $getData = MasterKegiatan::where('master_kegiatan.kode_p','LIKE',$id.'%')->orderBy('master_kegiatan.kode_i','ASC')->get();
        // dd($getData);die;
        return view('export.export_program', compact('getData','id'));
    }
}
