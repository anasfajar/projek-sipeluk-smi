<?php

namespace App\Exports;

use App\MasterKegiatan;
use App\MasterPegawai;
use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
Use \Maatwebsite\Excel\Sheet;

class cetakMasterSchejule implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $dataArr;
    public function __construct(string $kodeUser)
    {
      $this->dataArr = $kodeUser;
    }

    public function view(): View
    {
        // dd($this->dataArr);
        $kodeUser = $this->dataArr;
        // dd($kodeUser);
        $data = User::where('users.id',$kodeUser)->leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')->first();
        // dd($data);
        $getData = MasterKegiatan::where('master_kegiatan.user_id_create',$kodeUser)->orderBy('master_kegiatan.kode_i','ASC')->get();
        // dd($getData);die;
        return view('export.export_master_kabid', compact('getData','data'));
    }
}
