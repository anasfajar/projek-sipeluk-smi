<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() && Auth::user()->role === 'admin') {
            return redirect(route('admin.dashboard'));
        }
        elseif (Auth::check() && Auth::user()->role === 'kabid') {
            return redirect(route('kabid.dashboard'));
        }
        elseif (Auth::check() && Auth::user()->role === 'kasubid') {
            return redirect(route('kasubid.dashboard'));
        }

        return $next($request);
    }
}
