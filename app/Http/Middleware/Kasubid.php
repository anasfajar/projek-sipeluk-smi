<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Kasubid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role == 'kasubid') {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->role == 'kabid') {
            return redirect('/kabid/dashboard');
        }
        else if(Auth::check() && Auth::user()->role == 'admin') {
            return redirect('/admin/dashboard');
        }
        else
        {
          return redirect('/');
        }
    }
}
