<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MasterKegiatan;
use Carbon\Carbon;

use Excel;
use App\Exports\KasubidMasterPelaksana;


class KasubidController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('kasubid');
  }

  public function index()
  {
    $user = Auth::user();
    $getCount = MasterKegiatan::where('user_id',$user->id)->where('tipe_kode',3)->count();
    return view('pages.kasubid.dashboard', compact('getCount')); 
  }

//================================================ Master Table First =================================================//

  public function table_data_kasubid()
  {
    $user = Auth::user();
    $dt_program = MasterKegiatan::where('master_kegiatan.user_id',$user->id)
                                ->leftJoin('users','users.id','=','master_kegiatan.user_id')
                                ->leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')
                                ->orderBy('master_kegiatan.kode_i','ASC')
                                ->where('master_kegiatan.tipe_kode',3)
                                ->where('master_kegiatan.isActive',1)
                                ->get(
                                  [
                                    'master_kegiatan.kode_i',
                                    'master_kegiatan.aktivitas',
                                    'master_kegiatan.tgl_mulai',
                                    'master_kegiatan.tgl_selesai',
                                    'master_kegiatan.durasi',
                                    'master_kegiatan.tipe_kode',
                                  ]
                                );
    $data = array();
    $i = 1;
    // dd($dt_program);die;
    foreach($dt_program as $val)
    {
        $column['no']  = (string)$i++;
        $column['kodeInisial']  = (string)$val->kode_i;
        $column['aktivitas']  = (string)$val->aktivitas;
        $column['tglMulai']  = (string)$val->tgl_mulai;
        $column['tglSelesai']  = (string)$val->tgl_selesai;
        $column['durasiHari']  = (string)$val->durasi;
        $column['tipeKode']  = (string)$val->tipe_kode;
        $data[] = $column;
    }

    $response = array('data' => $data);

    echo json_encode($response);
  }



//================================================ Master Table First =================================================//

  public function get_count_tahapan($kodeInisial)
  {
    $data = MasterKegiatan::where('kode_i', 'LIKE', $kodeInisial.'%')->count();
    // $val = 00;    
    $count = $data;

    $response = ['data' => $count];
  
    return response()->json($response);
  }


//================================================ Master Table First =================================================//
  public function postDetilLangkah(Request $request)
  {
    $user = Auth::user();
    $checkData = MasterKegiatan::where('kode_i',$request->kodeInisial)->first();

    $newLangkah = new MasterKegiatan;

    $newLangkah->key_kode = $checkData->key_kode;
    $newLangkah->kode_p = $checkData->kode_p;
    $newLangkah->kode_k = $checkData->kode_k;
    $newLangkah->kode_t = $checkData->kode_t;
    $newLangkah->kode_i = $request->kodeInisial.'.'.$request->kodeDetilLangkah;
    $newLangkah->tipe_kode = 4;
    $newLangkah->user_id = $user->id;
    $newLangkah->user_id_create = Auth::user()->id;
    $newLangkah->aktivitas = $request->aktivitasLangkah;
    $newLangkah->tgl_mulai = $request->tglMulaiLangkah;
    $newLangkah->tgl_selesai = $request->tglSelesaiLangkah;
    $newLangkah->durasi = $request->durasiLangkah;

    $newLangkah->save();
    
    $response = ['data' => 'save'];
  
    return response()->json($response);
  }


//================================================ Master Table Pelaksana =================================================//
  public function getManagePelaksana()
  {
    $user = Auth::user();
    $getCount = MasterKegiatan::where('user_id',$user->id)->where('tipe_kode',3)->count();
    return view('pages.kasubid.ManagePelaksana',compact('getCount')); 
  }

  public function getTablePelaksana()
  {    
    $user = Auth::user();
    $date = Carbon::now()->format('Y-m-d');
    // echo $date;die;
    $dt_program = MasterKegiatan::leftJoin('users','users.id','=','master_kegiatan.user_id')
                                ->leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')
                                ->orderBy('master_kegiatan.kode_i','ASC')
                                ->where('master_kegiatan.user_id',$user->id)
                                ->where('master_kegiatan.tipe_kode', 4)
                                // ->whereNull('keterangan')
                                ->where('master_kegiatan.tgl_mulai','<=',Carbon::parse($date)->addDays(5))
                                ->get(
                                  [
                                    'master_kegiatan.kode_i',
                                    'master_kegiatan.aktivitas',
                                    'master_kegiatan.tgl_mulai',
                                    'master_kegiatan.tgl_selesai',
                                    'master_kegiatan.durasi',
                                    'master_kegiatan.tipe_kode',
                                    'master_kegiatan.persentase',
                                  ]
                                );
    $data = array();
    $i = 1;
    // dd($dt_program);die;
    foreach($dt_program as $val)
    {
        $column['no']  = (string)$i++;
        $column['kodeInisial']  = (string)$val->kode_i;
        $column['aktivitas']  = (string)$val->aktivitas;
        $column['tglMulai']  = (string)$val->tgl_mulai;
        $column['tglSelesai']  = (string)$val->tgl_selesai;
        $column['durasiHari']  = (string)$val->durasi;
        $column['tipeKode']  = (string)$val->tipe_kode;
        $column['persentase']  = (string)$val->persentase;
        $data[] = $column;
    }

    $response = array('data' => $data);

    echo json_encode($response);
  }

  public function postPelaksana(Request $request)
  { 
    
    $checkKey = MasterKegiatan::where('kode_i',$request->kodeInisial)->first();

    $checkData = MasterKegiatan::where('kode_i',$request->kodeInisial)->first();

    $checkData->key_kode = $checkKey->key_kode;
    $checkData->tgl_pelaksanaan = $request->tglMulaiPelaksana;
    $checkData->keterangan = $request->statusPelaksana;
    $checkData->persentase = $request->rangeData;
    $checkData->output = $request->aktivitasLangkah;      
    $gambarpath = 'Pelaksanaan/' . $request->tglMulaiPelaksana . '/Images';
    $dataImage = $this->Imageupload('Images' ,   $request->dokumenBukti, $gambarpath);
    $checkData->keterangan_durasi = $dataImage;
    $checkData->save();

    
    $gambarpath = 'Pelaksanaan/' . $request->tglMulaiPelaksana . '/Images';

    // $realImage = MasterKegiatan::where('kode_i',$request->kodeInisila)->first();

    // // $realImage->keterangan_durasi = $realImage;
    // // $realImage->save();



    return redirect()->back();
  }


  public function Imageupload($column, $request, $store_path)
  {
          $file = $request;
          $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();
          $path = $file->storeAs($store_path, $filename, 'public');
          return $path;


  }


  public function cetakPelaksana($kodeInisial)
  {
    return Excel::download(new KasubidMasterPelaksana($kodeInisial), 'Data Master Pelaksanaphp  -'.$kodeInisial.'.xlsx');
  }


  public function getPelaksanaView($kodeInisial)
  {
    $getData = MasterKegiatan::where('kode_i',$kodeInisial)->first();

    $data = ['data' => $getData];

    return response()->json($data);
  }

}
