<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MasterKegiatan;
use App\MasterPegawai;
use App\User;
use Auth;


use App\Exports\KabidMasterData;
use App\Exports\cetakMasterSchejule;
use Excel;

class KabidController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('kabid');
    }


    public function index()
    {
        // $count_user = User::all()->count('id');
        $count_program = MasterKegiatan::where('key_kode',0)->count('key_kode');
        $count_kegiatan = MasterKegiatan::whereNotIn('kode_k',['-'])->count('kode_k');
        $getKodeProgram = MasterKegiatan::where('key_kode',0)->get();
        return view('pages.kabid.dashboard', compact('count_program','count_kegiatan','getKodeProgram'));
    }


//================================================ Master Table First =================================================//

    public function table_manage_kabid()
    {
      $dt_program = MasterKegiatan::leftJoin('users','users.id','=','master_kegiatan.user_id')
                                  ->leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')
                                  ->orderBy('master_kegiatan.kode_i','ASC')
                                  ->whereNotnull('master_kegiatan.user_id')
                                  ->where('master_kegiatan.tipe_kode',3)
                                  ->where('master_kegiatan.isActive',1)
                                  ->get(
                                    [
                                      'master_pegawai.nama_pegawai',
                                      'master_kegiatan.kode_i',
                                      'master_kegiatan.aktivitas',
                                      'master_kegiatan.tgl_mulai',
                                      'master_kegiatan.tgl_selesai',
                                      'master_kegiatan.durasi',
                                    ]
                                  );
      $data = array();
      $i = 1;
      foreach($dt_program as $val)
      {
          $column['no']  = (string)$i++;
          $column['kodeInisial']  = (string)$val->kode_i;
          $column['aktivitas']  = (string)$val->aktivitas;
          $column['tglMulai']  = (string)$val->tgl_mulai;
          $column['tglSelesai']  = (string)$val->tgl_selesai;
          $column['durasiHari']  = (string)$val->durasi;
          $column['nameGuard']  = (string)$val->nama_pegawai;
          $data[] = $column;
      }

      $response = array('data' => $data);

      echo json_encode($response);
    }

    
    public function table_manage_guard()
    {
      $dt_program = MasterKegiatan::leftJoin('users','users.id','=','master_kegiatan.user_id')
                                  ->leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')
                                  ->orderBy('master_kegiatan.kode_i','ASC')
                                  ->whereNotnull('master_kegiatan.user_id')
                                  ->where('master_kegiatan.tipe_kode',2)
                                  ->where('master_kegiatan.isActive',1)
                                  ->get(
                                    [
                                      'master_pegawai.nama_pegawai',
                                      'master_kegiatan.kode_p',
                                      'master_kegiatan.kode_k',
                                      'master_kegiatan.kode_i',
                                      'master_kegiatan.aktivitas',
                                      'master_kegiatan.tgl_mulai',
                                      'master_kegiatan.tgl_selesai',
                                      'master_kegiatan.durasi',
                                    ]
                                  );
      $data = array();
      $i = 1;
      foreach($dt_program as $val)
      {
          $column['no']  = (string)$i++;
          $column['kodeProgram']  = (string)$val->kode_p;
          $column['kodeKegiatan']  = (string)$val->kode_k;
          $column['kodeInisial']  = (string)$val->kode_i;
          $column['aktivitas']  = (string)$val->aktivitas;
          $column['tglMulai']  = (string)$val->tgl_mulai;
          $column['tglSelesai']  = (string)$val->tgl_selesai;
          $column['durasiHari']  = (string)$val->durasi;
          $column['nameGuard']  = (string)$val->nama_pegawai;
          $data[] = $column;
      }

      $response = array('data' => $data);

      echo json_encode($response);
    }


// =============================================== Master Get Key Parent ===============================================//

    public function masterschedjule()
    {
      $count_program = MasterKegiatan::where('key_kode',0)->count('key_kode');
      $count_kegiatan = MasterKegiatan::whereNotIn('kode_k',['-'])->count('kode_k');
      $getKodeProgram = MasterKegiatan::where('key_kode',0)->get();
      return view('pages.kabid.masterschedjule', compact('count_program','count_kegiatan','getKodeProgram'));
    }

    public function get_parent_kode($kodeParent)
    {
      $get_key_kode = MasterKegiatan::where('master_kegiatan.key_kode',$kodeParent)
                                    ->whereNull('master_kegiatan.user_id')
                                    ->where('master_kegiatan.tipe_kode',2)
                                    ->get(
                                      [
                                        'master_kegiatan.kode_k',
                                        'master_kegiatan.aktivitas',
                                      ]
                                    );
      $response = ['data' => $get_key_kode];

      return response()->json($response);
      
    }

    
    public function get_parent_kode_kegiatan($kodeParent)
    {
      $get_key_kode = MasterKegiatan::where('master_kegiatan.key_kode',$kodeParent)
                                    ->whereNotNull('master_kegiatan.user_id')
                                    ->where('master_kegiatan.tipe_kode',2)
                                    ->get(
                                      [
                                        'master_kegiatan.kode_k',
                                        'master_kegiatan.aktivitas',
                                      ]
                                    );
      $response = ['data' => $get_key_kode];

      return response()->json($response);
      
    }

    public function getDataProgram($kodeParent)
    {
      $get_key_kode = MasterKegiatan::where('key_kode',0)->where('kode_p',$kodeParent)
                                    ->get(
                                      [
                                        'master_kegiatan.tgl_mulai',
                                        'master_kegiatan.tgl_selesai',
                                        'master_kegiatan.durasi',
                                      ]
                                    );
      $response = ['dt' => $get_key_kode];

      return response()->json($response);
    }


// =============================================== Master Get Count Kegiatan ===============================================//
  
    public function get_count_kegiatan($kodeKegiatan)
    {
      $data = MasterKegiatan::where('kode_k','LIKE',$kodeKegiatan.'%')->where('tipe_kode',3)->count('kode_t');

      $val = 00;
      $count = $val.$data+1;
      // echo $count;die;

     $response = ['data' => $count];

     return response()->json($response);
    }

    public function get_date_kegiatan($kodeKegiatan)
    {
       $data = MasterKegiatan::where('kode_k',$kodeKegiatan)->select('tgl_mulai','tgl_selesai')->first();

       $response = ['data' => $data];

       return response()->json($response);
    }

// =============================================== Master Add Guard =========================================================//

    public function addGuard()
    {
      $count_program = MasterKegiatan::where('key_kode',0)->count('key_kode');
      $count_kegiatan = MasterKegiatan::whereNotIn('kode_k',['-'])->count('kode_k');
      $getKodeProgram = MasterKegiatan::where('key_kode',0)->get();
      $getGuard = User::leftJoin('master_pegawai','master_pegawai.id_user','=','users.id')
                      ->where('users.role','kasubid')
                      ->get(
                        [
                          'master_pegawai.nama_pegawai',
                          'users.id',
                        ]
                      );
      return view('pages.kabid.penanggungjawab', compact('count_program','count_kegiatan','getKodeProgram','getGuard'));      
    }
    
    public function postGuard(Request $request)
    {
      // echo  $request->kode_program;die;
      $check = MasterKegiatan::where('key_kode',$request->kodeProgram)->where('kode_k',$request->kodeKegiatan)->first();
      
      if(empty($check))
      {
        $response = ['data' => 'empty'];
   
        return response()->json($response);
      }
      else
      {
        $update = MasterKegiatan::where('key_kode',$request->kodeProgram)->where('kode_k',$request->kodeKegiatan)->first();

        $update->user_id = $request->guardKegiatan;
        $update->save();

        
        $response = ['data' => 'save'];
   
        return response()->json($response);

      }
    }
    
// =============================================== Master Delete Guard =========================================================//
  public function deleteGuard($kodeInisial)
  {
    
    $update = MasterKegiatan::where('kode_i',$kodeInisial)->first();

    $update->user_id = NULL;
    $update->save();

    
    $response = ['data' => 'save'];

    return response()->json($response);


  }



  
// =============================================== Master Delete Guard =========================================================//

  public function postTahapan(Request $request)
  {
    // dd(Auth::user()->id);/
    $getGuard = MasterKegiatan::where('kode_p',$request->kodeProgram)
                                  ->where('kode_k',$request->kodeKegiatan)
                                  ->first();
    $postProgram = MasterKegiatan::where('key_kode',0)
                                 ->where('kode_p', $request->kodeProgram)
                                 ->first();
    
    $postProgram ->tgl_mulai = $request->tglMProgram;
    $postProgram ->tgl_selesai = $request->tglSProgram;
    $postProgram ->durasi = $request->durasiProgram;
    $postProgram->save();

    $postKegiatan = MasterKegiatan::where('kode_p',$request->kodeProgram)
                                  ->where('kode_k',$request->kodeKegiatan)
                                  ->first();
    
    $postKegiatan->tgl_mulai = $request->tglMKegiatan;
    $postKegiatan->tgl_selesai = $request->tglSKegiatan;
    $postKegiatan->durasi = $request->durasiKegiatan;
    $postKegiatan->save();
    
    $newTahapan = new MasterKegiatan;

    $newTahapan->key_kode = $request->kodeProgram;
    $newTahapan->kode_p = $request->kodeProgram;
    $newTahapan->kode_k = $request->kodeKegiatan;
    $newTahapan->kode_t = $request->kodeTahapan;
    $newTahapan->kode_i = $request->kodeProgram.'.'.$request->kodeKegiatan.'.'.$request->kodeTahapan;
    $newTahapan->tipe_kode = 3;
    $newTahapan->user_id = $getGuard->user_id;
    $newTahapan->user_id_create = Auth::user()->id;
    $newTahapan->aktivitas = $request->aktivitasTahapan;
    $newTahapan->tgl_mulai = $request->tglMTahapan;
    $newTahapan->tgl_selesai = $request->tglSTahapan;
    $newTahapan->durasi = $request->durasiTahapan;

    $newTahapan->save();

    $response = ['data' => 'save'];

    return response()->json($response);

                                 
  }
  
// =============================================== Master Delete Guard =========================================================//
  public function deleteMaster($kodeInisial)
  {
    $deleteMaster = MasterKegiatan::where('kode_i',$kodeInisial)->delete();

    $response = ['data' => 'save'];

    return response()->json($response);
  }

  public function viewMaster($kodeInisial)
  {
    $viewMaster = MasterKegiatan::where('kode_i',$kodeInisial)->select(
                                                                        [
                                                                          'master_kegiatan.kode_i',
                                                                          'master_kegiatan.aktivitas',
                                                                          'master_kegiatan.tgl_mulai',
                                                                          'master_kegiatan.tgl_selesai',
                                                                          'master_kegiatan.durasi'
                                                                        ]
                                                                      )->first();

    $jsonParsing = ['data' => $viewMaster ];

    return response()->json($jsonParsing);
  }




  public function cetakDataMaster($kodeInisial)
  {
    return Excel::download(new KabidMasterData($kodeInisial), 'Data Master Kabid -'.$kodeInisial.'.xlsx');
  }

  public function cetakMasterSchejule($kodeUser)
  {
    // dd($kodeUser);
    return Excel::download(new cetakMasterSchejule($kodeUser), 'Master Kegiatan Kabid -'.$kodeUser.'.xlsx');    
  }
  
}
