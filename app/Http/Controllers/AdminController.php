<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;
use App\MasterPegawai;
use App\MasterKegiatan;
use App\Exports\ExportProgram;
use App\Exports\ExportKegiatan;
use App\Exports\ExportTahapan;

use Excel;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

//================================================ Program Index Manage =============================================================//
    public function index()
    {
        $count_user = User::all()->count('id');
        $count_program = MasterKegiatan::where('key_kode',0)->count('key_kode');
        $count_kegiatan = MasterKegiatan::where('tipe_kode',2)->count();
        // echo $sum_user;die;
        return view('pages.admin.dashboard',compact('count_user','count_program','count_kegiatan'));
    }

//================================================ Program Manage Program ============================================================//
    public function table_manage_kegiatan()
    {
      $dt_program = MasterKegiatan::orderBy('kode_p','ASC')->orderBy('kode_i','ASC')->get();
      // dd($dt_program);die;
      $data = array();
      $i = 1;
      foreach($dt_program as $val)
      {
          $column['no']  = (string)$i++;
          if($val->key_kode == 0)
          {
            $column['kodeProgram']  = (string)$val->kode_p;            
          }
          else
          {
            $column['kodeProgram']  = (string)$val->key_kode;
          }
          $column['p_kode']  = (string)$val->kode_p;
          $column['kodeInisial']  = (string)$val->kode_i;
          $column['aktivitas']  = (string)$val->aktivitas;
          if($val->key_kode == 0)
          {
            $column['isActive'] = ' ';
          }
          else
          {
            if($val->isActive == 1)
            {
              $column['isActive'] = 'Aktif';
  
            }
            else
            {
              $column['isActive'] = 'Nonaktif';
            }

          }
          $column['k_kode']  = (string)$val->kode_k;
          $column['t_kode']  = (string)$val->tipe_kode;

          $data[] = $column;
      }

      $response = array('data' => $data);

      echo json_encode($response);
    }

    public function post_manage_kegiatan(Request $request)
    {
        $check_data = MasterKegiatan::where('key_kode',$request->m_kode)->where('kode_p', $request->kode)->first();
        if(!empty($check_data))
        {

        }
        else
        {
          $data = new MasterKegiatan;
    
          $data->key_kode = $request->m_kode;
          $data->kode_p = $request->kode;
          $data->tipe_kode = 1;
          $data->isActive = 1;
          $data->aktivitas = $request->aktivitas;
    
          $data->save(); 

          $response = ['data' => 'sukses'];
          return response()->json($response);         
        }
      
    }

    public function edit_manage_kegiatan(Request $request)
    {
      $e_data = MasterKegiatan::where('kode_p',$request->b_kode)->first();
      if(empty($e_data))
      {
        $response = ['data' => 'error'];
        return response()->json($response);
      }
      else
      {
        $data_first = MasterKegiatan::where('kode_p',$request->b_kode)->first();
         
        $data_first->key_kode = 0;
        $data_first->kode_p = $request->kode;
        $data_first->aktivitas = $request->aktivitas;
        $data_first->save();


        $data_second = MasterKegiatan::where('key_kode',$request->b_kode)->get();
        if(empty($data_second))
        {

        }
        else
        {
          $data_second = MasterKegiatan::where('key_kode',$request->b_kode)->get();
          foreach($data_second as $data_edit)
          {
            if($data_edit->kode_t)
            {
              $kode_real = $request->kode.'.'. $data_edit->kode_k.'.'.$data_edit->kode_t;
    
              $data_edit->key_kode = $request->kode;  
              $data_edit->kode_p = $request->kode;
              $data_edit->kode_k = $data_edit->kode_k;
              $data_edit->kode_t = $data_edit->kode_t;
              $data_edit->kode_i = $kode_real;
              $data_edit->aktivitas = $data_edit->aktivitas;
              $data_edit->isActive = 1;
              $data_edit->tgl_mulai = $data_edit->tgl_mulai;
              $data_edit->tgl_selesai = $data_edit->tgl_selesai;
              $data_edit->durasi = $data_edit->durasi; 
              $data_edit->save();

            }
            else
            {
              $kode_real = $request->kode.'.'. $data_edit->kode_k;
    
              $data_edit->key_kode = $request->kode;  
              $data_edit->kode_p = $request->kode;
              $data_edit->kode_k = $data_edit->kode_k;
              $data_edit->kode_t = $data_edit->kode_t;
              $data_edit->kode_i = $kode_real;
              $data_edit->aktivitas = $data_edit->aktivitas;
              $data_edit->isActive = 1;
              $data_edit->tgl_mulai = $data_edit->tgl_mulai;
              $data_edit->tgl_selesai = $data_edit->tgl_selesai;
              $data_edit->durasi = $data_edit->durasi; 
              $data_edit->save();

            }
            
          }

        }

        $response = ['data' => 'sukses'];

        return response()->json($response);      
      }


    }

    public function change_manage_aktif($kodeInisial)
    {
      $getData = MasterKegiatan::where('kode_i','LIKE',$kodeInisial.'%')->get();
      for($i=0;$i<sizeOf($getData);$i++)
      {
        $getData[$i]->isActive = 1;
        $getData[$i]->save();
      }

      $data = ['data' => 'Success'];

      return response()->json($data);
    }

    public function change_manage_nonaktif($kodeInisial)
    {
      $getData = MasterKegiatan::where('kode_i','LIKE',$kodeInisial.'%')->get();
      for($i=0;$i<sizeOf($getData);$i++)
      {
        $getData[$i]->isActive = 0;
        $getData[$i]->save();
      }

      $data = ['data' => 'Success'];

      return response()->json($data);
    }
    
//================================================ Program Manage Kegiatan =================================================//

public function post_add_kegiatan(Request $request)
{
  $kode_data = $request->kode_p.'.'.$request->kode_k;
  $check = MasterKegiatan::where('kode_i',$kode_data)->first();
  if($check)
  {
    $response = ['dt' => 'error'];

    return response()->json($response);
  }
  else
  {
    $keg_new = new MasterKegiatan;
    
    $keg_new->key_kode = $request->kode_p;
    $keg_new->kode_p = $request->kode_p;
    $keg_new->kode_i = $kode_data;
    $keg_new->kode_k = $request->kode_k;
    $keg_new->kode_t = null;
    $keg_new->tipe_kode = 2;
    $keg_new->isActive = 1;
    $keg_new->aktivitas = $request->aktivitas_k;
    $keg_new->save();
    
    $response = ['dt' => 'success'];

    return response()->json($response);
  }
}

public function post_edit_kegiatan(Request $request)
{
    
    $editDatas = MasterKegiatan::where('kode_k',$request->kode_m)->get();
    foreach($editDatas as $editData)
    {
      if($editData->kode_t)
      {
        $kode_data = $request->kode_p.'.'.$request->kode_k.'.'.$editData->kode_t;
  
        $editData->key_kode = $request->kode_p;
        $editData->kode_i = $kode_data;
        $editData->kode_k = $request->kode_k;
        $editData->kode_p = $request->kode_p;
        $editData->kode_t = null;
        $editData->tipe_kode = 3;
        $editData->isActive = 1;
        $editData->aktivitas = $request->aktivitas_k;
        $editData->save();      
  
      }
      else
      {
        $kode_data = $request->kode_p.'.'.$request->kode_k;
    
        $editData->key_kode = $request->kode_p;
        $editData->kode_i = $kode_data;
        $editData->kode_k = $request->kode_k;
        $editData->kode_p = $request->kode_p;
        $editData->kode_t = null;
        $editData->tipe_kode = 2;
        $editData->isActive = 1;
        $editData->aktivitas = $request->aktivitas_k;
        $editData->save();      
      }

    }
    
    $response = ['dt' => 'success'];

    return response()->json($response);
  
}


//================================================ User Manage =============================================================//
    public function manage_user()
    {
        $data_user = User::leftJoin('master_pegawai', 'users.id', '=', 'master_pegawai.id_user')
                     ->select(
                         [
                             'users.id',
                             'users.name',
                             'users.email',
                             'users.role',
                             'master_pegawai.nip_pegawai',
                             'master_pegawai.nama_pegawai',
                             'master_pegawai.jabatan_pegawai',
                             'master_pegawai.keterangan_pegawai',
                         ]
                     )
                     ->get();
                     

        return view('pages.admin.manage_user', compact('data_user'));
    }
    

    public function post_user(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string'],
            'role' => ['required','in:admin,kabid,kasubid'],
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
            $post_user = new User;
            $post_user->name = $request->name;
            $post_user->email = $request->email;
            $post_user->password = bcrypt($request->password);
            $post_user->role = $request->role;
            $post_user->save();

            $post_data = new MasterPegawai;

            $post_data->id_user = $post_user->id;
            $post_data->nip_pegawai = $request->nip;
            $post_data->nama_pegawai = $request->nama_pegawai;
            $post_data->jabatan_pegawai = $request->jabatan;
            $post_data->keterangan_pegawai = $request->keterangan;
            $post_data->save();
            

        return redirect()->back()->with('status','Berhasil Menambahkan User Baru.');
    }



    public function manage_edit_post(Request $request)
    {   
        // var_dump($request->all());die;
        $update_user = User::where('id',$request->id)->first();

        $update_user->name = $request->name;
        $update_user->email = $request->email;
        $update_user->role =  $request->role;
        $update_user->save();
        
        $update_data = MasterPegawai::where('id_user',$request->id)->first();
        if(empty($update_data))
        {
            $post_data = new MasterPegawai;

            $post_data->id_user = $request->id;
            $post_data->nip_pegawai = $request->nip;
            $post_data->nama_pegawai = $request->nama_pegawai;
            $post_data->jabatan_pegawai = $request->jabatan;
            $post_data->keterangan_pegawai = $request->keterangan;
            $post_data->save();
        }
        else
        {
            $update_data->nip_pegawai = $request->nip;
            $update_data->nama_pegawai = $request->nama_pegawai;
            $update_data->jabatan_pegawai = $request->jabatan;
            $update_data->keterangan_pegawai = $request->keterangan;
            $update_data->save();
        }


        return redirect(route('admin.manage_user'))->with('update','Berhasil update user.');

    }

    public function repassword(Request $request)
    {
        $repassword_user = User::where('id',$request->id)->first();
        $repassword_user->password = bcrypt($request->password);
        $repassword_user->save();

        return redirect(route('admin.manage_user'))->with('repassword','Berhasil mengubah password user.');
    }

    public function manage_delete_user($id)
    {
        $delete_user = User::where('id',$id)->first();
        $delete_user->delete();

        return redirect(route('admin.manage_user'))->with('delete','Berhasil menghapus user.');
    }



//================================================ Export Manage =============================================================//

    public function cetakDataProgram($kodeInisial)
    {
      
      return Excel::download(new ExportProgram($kodeInisial), 'Data Tahapan -'.$kodeInisial.'.xlsx');
    }

    public function cetakDataKegiatan($kodeInisial)
    {
      
      return Excel::download(new ExportKegiatan($kodeInisial), 'Data Tahapan -'.$kodeInisial.'.xlsx');
    }

    public function cetakDataTahapan($kodeInisial)
    {
      
      return Excel::download(new ExportTahapan($kodeInisial), 'Data Tahapan -'.$kodeInisial.'.xlsx');
    }

    
}
