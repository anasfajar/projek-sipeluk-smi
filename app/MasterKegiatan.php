<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterKegiatan extends Model
{
    protected $table = 'master_kegiatan';
    protected $fillable = ['keterangan_durasi'];

    public function children(){
        return $this->hasMany( 'App\MasterKegiatan', 'parent_kode','kode');
    }
    
    public function parent(){
        return $this->belongsTo( 'App\MasterKegiatan', 'parent_kode' );
    }
}
