<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterProgram extends Model
{
    protected $table = 'master_program';
    
    public function kegiatan()
    {
        return $this->hasMany('App\MasterKegiatan','id_program');
    }
}
