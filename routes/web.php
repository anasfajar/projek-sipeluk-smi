<?php

/*
|--------------------------------------------------------------------------
| Web Routes Sipeluk
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//================================================ Manage Perogram & Kegiatan ===============================================//

Route::get('/', 'StaticController@index');
Route::auth(['register' => false]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function(){
    Route::get('/dashboard','AdminController@index')->name('admin.dashboard');   
//================================================ Manage User =============================================================//
    Route::get('/manage_user', 'AdminController@manage_user')->name('admin.manage_user');
    Route::post('/post_user','AdminController@post_user')->name('admin.manage_user.post');
    Route::post('/repassword','AdminController@repassword')->name('admin.manage_user.repassword');
    Route::post('/manage_edit_post','AdminController@manage_edit_post')->name('admin.manage_edit.post');
    Route::get('/manage_delete_user/{id}','AdminController@manage_delete_user')->name('admin.manage_delete_user');
    
//================================================ Manage Perogram & Kegiatan ===============================================//
    Route::get('/table_manage_kegiatan','AdminController@table_manage_kegiatan');
    Route::post('/post_manage_kegiatan','AdminController@post_manage_kegiatan');
    Route::post('/edit_manage_kegiatan','AdminController@edit_manage_kegiatan');
    Route::get('/change_manage_aktif/{kodeInisial}','AdminController@change_manage_aktif');
    Route::get('/change_manage_nonaktif/{kodeInisial}','AdminController@change_manage_nonaktif');

//================================================ Manage Kegiatan =========================================================//
    Route::post('/post_add_kegiatan','AdminController@post_add_kegiatan');
    Route::post('/post_edit_kegiatan','AdminController@post_edit_kegiatan');


//================================================ Manage Export Data ======================================================//

    Route::get('/cetakDataProgram/{kodeInisial}','AdminController@cetakDataProgram');
    Route::get('/cetakDataKegiatan/{kodeInisial}','AdminController@cetakDataKegiatan');
    Route::get('/cetakDataTahapan/{kodeInisial}','AdminController@cetakDataTahapan');
    

    
});


Route::group(['prefix' => 'kabid', 'middleware' => 'kabid'], function() {
    Route::get('/dashboard','KabidController@index')->name('kabid.dashboard');
    Route::get('/masterschedjule','KabidController@masterschedjule');
//================================================ Manage Guard ======================================================//

    Route::get('/get_guard','KabidController@get_guard');    
    Route::get('/addGuard','KabidController@addGuard'); 
    Route::post('/postGuard','KabidController@postGuard');
    Route::get('/deleteGuard/{kodeInisial}','KabidController@deleteGuard');
//================================================ Manage Table First ================================================//

    Route::get('/table_manage_kabid','KabidController@table_manage_kabid');
    Route::get('/table_manage_guard','KabidController@table_manage_guard');
    Route::get('/get_parent_kode/{kodeParent}','KabidController@get_parent_kode');
    Route::get('/get_parent_kode_kegiatan/{kodeParent}','KabidController@get_parent_kode_kegiatan');
    Route::get('/get_count_kegiatan/{kodeKegiatan}','KabidController@get_count_kegiatan');
    Route::get('/getDataProgram/{kodeParent}','KabidController@getDataProgram');
    Route::get('/get_date_kegiatan/{kodeKegiatan}','KabidController@get_date_kegiatan');
    Route::get('/cetakMasterSchejule/{kodeUser}','KabidController@cetakMasterSchejule');
//================================================ Manage PostTahapan ================================================//
    Route::post('/postTahapan','KabidController@postTahapan');
    Route::get('/deleteMaster/{kodeInisial}','KabidController@deleteMaster');
    Route::get('/viewMaster/{kodeInisial}','KabidController@viewMaster');


    
//================================================ Manage Export Data ================================================//

  Route::get('/cetakDataMaster/{kodeInisial}','KabidController@cetakDataMaster');

    
});







Route::group(['prefix' => 'kasubid', 'middleware' => 'kasubid'], function() {
  Route::get('/dashboard','KasubidController@index')->name('kasubid.dashboard');
//================================================ Manage Table ======================================================//

  Route::get('/table_data_kasubid','KasubidController@table_data_kasubid'); 
  Route::get('/get_count_tahapan/{kodeInisial}','KasubidController@get_count_tahapan');
//================================================ Manage Table ======================================================//
  Route::post('/postDetilLangkah','KasubidController@postDetilLangkah');

  
//================================================ Manage Pelaksana ==================================================//

  Route::get('/getManagePelaksana','KasubidController@getManagePelaksana');
  Route::get('/getTablePelaksana','KasubidController@getTablePelaksana');
  Route::post('/postPelaksana','KasubidController@postPelaksana');

    
//================================================ Cetak  Pelaksana ==================================================//

  Route::get('/cetakPelaksana/{kodeInisial}','KasubidController@cetakPelaksana');
  Route::get('/getPelaksanaView/{kodeInisial}','KasubidController@getPelaksanaView');


});
