<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document Tahapan </title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

  <table>
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td colspan="3"><h1> <b>Dokumen Pelaksana Kegiatan</b> </h1> </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Instansi </td>
        <td>: </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Bidang </td>
        <td colspan="5">: {{$data->keterangan_pegawai}}</td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td>Kepala Sub Bidang </td>
          <td>: {{$data->nama_pegawai}}</td>
        </tr>
    </tbody>
  </table>
  <table class="table">
    <tbody >
      <tr>
        <td rowspan="2">&nbsp;</td>
        <td rowspan="2">KODE KEGIATAN / TAHAPAN KEGIATAN</td>
        <td rowspan="2">AKTIVITAS KEGIATAN / LANGKAH TAHAPAN KEGIATAN</td>
        <td rowspan="2">OUTPUT</td>
        <td colspan="1">PELAKSANA KEGIATAN</td>
        <td colspan="2">MASTER SCHEDULE KEGIATAN</td>
        <td rowspan="2">DURASI</td>
        <td rowspan="2">KETERANGAN</td>
        <td rowspan="2">PERSENTASE KESESUAIAN</td>
      </tr>
      <tr>
        <td >TANGGAL PELAKSANA</td>
        <td >TANGGAL MULAI</td>
        <td >TANGGAL SELESAI</td>
      </tr>
      @foreach ($getData as $item)
        <tr>
          <td>&nbsp;</td>
          <td>{{$item->kode_i}}</td>
          <td>{{$item->aktivitas}}</td>
          <td>{{$item->output}}</td>
          <td>{{$item->tgl_pelaksanaan}}</td>
          <td>{{$item->tgl_mulai}}</td>
          <td>{{$item->tgl_selesai}}</td>
          <td>{{$item->durasi}}</td>
          @if($item->keterangan == 1)
          <td>On Proses</td>
          @elseif($item->keterangan == 2)
          <td>On Time</td>
          @elseif($item->keterangan == 3)
          <td>Over Time</td>
          @else
          <td></td>
          @endif
          <td>{{$item->persentase}}</td>
        </tr>          
      @endforeach
    </tbody>
  </table>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>