<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    
    {{-- vendor css --}}
    {{-- <link rel="stylesheet" href="/admin/admin/vendors/bootstrap/dist/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" href="/admin/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/admin/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/admin/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/admin/vendors/jqvmap/dist/jqvmap.min.css">


    {{-- default css bootstrap --}}
    <link rel="stylesheet" href="/css/app.css">
    
    {{-- style css --}}
    <link rel="stylesheet" href="/admin/assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


    {{-- font style --}}
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        .modal-open .modal
        {
            overflow-y:hidden;
        }
    </style>

</head>
<body>
    @include('include.admin.navbar_master')
  <div class="container-fluid p-0 m-0">
    @yield('content')
  </div>

  {{--  Js Default --}}
  <script src="/js/app.js"></script>
  
  {{-- Js Custom And Vendor --}}
  {{-- <script src="/admin/vendors/jquery/dist/jquery.min.js"></script>
  <script src="/admin/vendors/popper.js/dist/umd/popper.min.js"></script> --}}
  {{-- <script src="/admin/assets/js/main.js"></script>
  <script src="/admin/assets/js/dashboard.js"></script> --}}
  <script src="/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/admin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="/admin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
  <script src="/admin/vendors/jszip/dist/jszip.min.js"></script>
  <script src="/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
  <script src="/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="/admin/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
  <script src="/admin/assets/js/init-scripts/data-table/datatables-init.js"></script>
</body>
</html>