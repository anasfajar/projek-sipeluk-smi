<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
		<style>
        body{
          background-color: #f8f8f8;
        }
        .text-front
        {
          margin: 20% 0;
          text-align: center;
        }
  
        .text-front h2
        {
          font-size: 58px;
          font-weight: bold;
          text-shadow: 0px 10px 24px -10px rgba(0,0,0,0.75);
        }
      </style>
</head>
<body>
    <div id="app">
        <div class="container-fluid p-0 m-0">
            <nav class="navbar navbar-expand-lg container-fluid navbar-dark" style="background-color:#45969b">
                <div class="collapse navbar-collapse container p-2" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="/"> SIPELUK</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    </ul>
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                @if(Auth::user()->role === 'admin')
                                    <a class="btn btn-dark btn-lg my-2 my-sm-0" href="{{ route('admin.home') }}">Dashboard</a>
                                    <a class="btn btn-warning btn-lg my-2 my-sm-0" href="{{ route('logout') }}">Keluar</a>

                                @elseif(Auth::user()->role === 'kabid')
                                <a class="btn btn-dark btn-lg my-2 my-sm-0" href="{{ route('kabid.dashboard') }}">Dashboard</a>
                                <a class="btn btn-warning btn-lg my-2 my-sm-0" href="{{ route('logout') }}">Keluar</a>

                                @elseif(Auth::user()->role === 'kasubid')
                                <a class="btn btn-dark btn-lg my-2 my-sm-0" href="{{ route('kasubid.dashboard') }}">Dashboard</a>
                                <a class="btn btn-warning btn-lg my-2 my-sm-0" href="{{ route('logout') }}">Keluar</a>

                                @endif
                            @else
                                {{--  <a href="{{ route('login') }}">Login</a>  --}}
                                <a class="btn btn-warning btn-lg my-2 my-sm-0" href="/" >Kembali</a>
                            @endauth
                        </div>
                    @endif
                </div>
            </nav>
        </div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
