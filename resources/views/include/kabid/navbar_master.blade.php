
{{-- Start left Navbar Admin --}}
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
  
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{route('kabid.dashboard')}}">SIPELUK</a>
            <a class="navbar-brand hidden" href="{{route('kabid.dashboard')}}"><img src="/admin/images/logo2.png" alt="Logo"></a>
        </div>
  
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <h3 class="menu-title">Nama : {{Auth::user()->name}}</h3>
                <li class="active">
                    <a href="{{route('kabid.dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i> Dashboard </a>
                </li>
  
                &nbsp;
                <h3 class="menu-title">Pengaturan</h3>
                <li class="active">
                    <a href="/kabid/addGuard"> <i class="menu-icon fa fa-user"></i> Penanggung Jawab </a>
                </li>
                <li class="active">
                    <a href="/kabid/masterschedjule"> <i class="menu-icon fa fa-pencil"></i> Master Schedule </a>
                </li>
            </ul>
        </div>
    </nav>
  </aside>
  {{-- End Left Navbar --}}