
{{-- Start left Navbar Admin --}}
<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">

      <div class="navbar-header">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="{{route('admin.dashboard')}}">SIPELUK</a>
          <a class="navbar-brand hidden" href="{{route('admin.dashboard')}}"><img src="/admin/images/logo2.png" alt="Logo"></a>
      </div>

      <div id="main-menu" class="main-menu collapse navbar-collapse">
          <ul class="nav navbar-nav">
              <h3 class="menu-title">Nama : {{Auth::user()->name}}</h3>
              <li class="active">
                  <a href="{{route('kasubid.dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i> Dashboard </a>
              </li>

              &nbsp;
              <h3 class="menu-title">Pengaturan</h3>
              <li class="">
                  <a href="/kasubid/getManagePelaksana"> <i class="menu-icon fa fa-user"></i>Manage Pelaksana </a>
              </li>
          </ul>
      </div>
  </nav>
</aside>
{{-- End Left Navbar --}}