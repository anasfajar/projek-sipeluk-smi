
@extends('layouts.kabid.master')

@section('title','Dashboard Kabid')
<style>
.modal-add {
    max-width:70% !important;
    margin:0 auto;
    /*add what you want here*/
}
</style>

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
        <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Program</div>
                          <div class="stat-digit">{{$count_program}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Kegiatan</div>
                          <div class="stat-digit">{{$count_kegiatan}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <strong class="card-title" style="color:white">Table Penanggung Jawab Kegiatan</strong>
                    <button type="button" class="btn btn-success float-right" id="BtntambahGuard" data-toggle="modal" data-target="#tambahGuard">Tambah Penanggung Jawab</button>
                </div>
                <div class="card-body">
                    <table class="table table-striped"  id="table_master" >
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode Program </th>
                                <th scope="col">Kode Kegiatan </th>
                                <th scope="col">Kode </th>
                                <th scope="col">Nama Kegiatan</th>
                                <th scope="col">Penanggung Jawab</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
      </div>



  {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}

  <div class="modal  fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tambahGuard" aria-hidden="true" id="tambahGuard">
    <div class="modal-dialog modal-add">
      <div class="modal-content">
          <div class="modal-header">
            <h2>Tambah Penanggung Jawab Kegiatan</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-lg-4 col-sm-12">
                <label for="">Kode Program</label>
                <select class="custom-select" name="" id="kodeProgram">
                  <option value="0"> -- Pilih Kode Program --  </option>
                    @foreach ($getKodeProgram as $item)
                      <option value="{{$item->kode_p}}">- {{$item->kode_p}} - {{$item->aktivitas}} -</option>                                        
                    @endforeach
                </select>
              </div>
              <div class="form-group col-lg-4 col-sm-12">
                <label for="">Kode Kegiatan</label>
                <select class="custom-select" name="" id="kodeKegiatan">
                  <option value="0" disabled selected hidden> -- Pilih Kode Kegiatan --  </option>
                </select>
              </div>
              <div class="form-group col-lg-4 col-sm-12">
                <label for="">Penanggung Jawab</label>
                <select class="custom-select" name="" id="guardKegiatan">
                  <option value="0" disabled selected hidden> -- Pilih Penanggung Jawab--  </option>
                    @foreach ($getGuard as $item)
                      <option value="{{$item->id}}"> {{$item->nama_pegawai}} </option>                      
                    @endforeach
                </select>
              </div>
            </div>

          </div>
          <div class="modal-footer" id="footerAction">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-success" id="btnSimpanData">Simpan</button>            
          </div>
      </div>
    </div>
  </div>


      
</div>
<script>
  $(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

      table = $('#table_master').DataTable({
        "processing": true,
        ajax: {
            url: '/kabid/table_manage_guard',
            type:'get',
        },
      "columns" : [
                { "data" : "no" },
                { "data" : "kodeProgram" },
                { "data" : "kodeKegiatan" },
                { "data" : "kodeInisial" },
                { "data" : "aktivitas" },
                { "data" : "nameGuard" },
            ],
        "columnDefs": [
          {
            "targets": 6 ,
            render:function(data,type,row,meta){
                return '<button class="btn btn-danger" id="deleteGuardBtn"><span class="ti-close"></span></button> ';
            }
          },
        ]    
        
      });
      
      $('#BtntambahGuard').on('click', function(){
        // kode Program reset
        $('#kodeProgram').val(0);
        $('#kodeKegiatan').val(0);

        
      })

      $('#table_master tbody').on('click','#deleteGuardBtn', function(){
        var data = table.row( $(this).parents('tr') ).data();
        kodeInisial = data.kodeInisial
        $.ajax({
          url : '/kabid/deleteGuard/'+kodeInisial,
          type : 'get',
          success:function(data)
          {
            location.reload();
          }
        })
      })


      $('#kodeProgram').on('change', function(){
        $('#kodeKegiatan').empty();

        if($('#kodeProgram').val() == 0)
        {

          $('#kodeProgram').val(0);
          $('#kodeKegiatan').append($('<option>', {
              value: 0,
              text: '-- Pilih Kode Kegiatan --'
          }));    
        }
        else
        {
          $('#kodeKegiatan').val(0);    
          
          kodeParent = $('#kodeProgram').val();
          $.ajax({
              url:'/kabid/get_parent_kode/'+kodeParent,
              type:'get',
              success:function(data)
              {
                dt = data.data 
                $('#kodeKegiatan').append($('<option>', {
                    value: 0,
                    text: '-- Pilih Kode Kegiatan --'
                }));            
                $.each(dt, function (index, item) {
                    $('#kodeKegiatan').append($('<option>', { 
                        value: item.kode_k,
                        text : item.kode_k +' - '+ item.aktivitas,
                        placeholder: {
                          id: '1', // the value of the option
                          text: 'Select an option'
                        },
                    }));
                });
              }
          });
        }
      })


      $('#btnSimpanData').on('click', function(){
        kodeProgram = $('#kodeProgram').val();
        kodeKegiatan = $('#kodeKegiatan').val();
        guardKegiatan = $('#guardKegiatan').val();

        $.ajax({
          url : '/kabid/postGuard',
          type : 'POST',
          data : {kodeProgram:kodeProgram,kodeKegiatan:kodeKegiatan,guardKegiatan:guardKegiatan},
          success:function(data)
          {
            location.reload()
          }
        });
      })

    });
</script>

@endsection