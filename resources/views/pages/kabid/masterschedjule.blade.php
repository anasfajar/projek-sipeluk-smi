@extends('layouts.kabid.master')

@section('title','Dashboard Kabid')
<style>
.modal-add {
    max-width:70% !important;
    margin:0 auto;
    /*add what you want here*/
}
</style>

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
        <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Program</div>
                          <div class="stat-digit">{{$count_program}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Kegiatan</div>
                          <div class="stat-digit">{{$count_kegiatan}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <strong class="card-title" style="color:white">Tabel Master Schedule 1</strong>
                    {{-- <span>{{Auth::user()->id}}</span> --}}
                    <a href="/kabid/cetakMasterSchejule/{{Auth::user()->id}}" class="btn btn-primary float-right">Export All Data</a>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <button type="button" class="btn btn-success float-right" id="BtntambahMasterSchedule" data-toggle="modal" data-target="#tambahMasterSchedule">Tambah Jadwal</button>
                </div>
                <div class="card-body">
                    <table class="table table-striped"  id="table_master" >
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Tahapan Kegiatan</th>
                                <th scope="col">Tanggal Mulai</th>
                                <th scope="col">Tanggal Selesai</th>
                                <th scope="col">Durasi</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
      </div>



  {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}

  <div class="modal  fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tambahMasterSchedule" aria-hidden="true" id="tambahMasterSchedule">
    <div class="modal-dialog modal-add">
      <div class="modal-content">
          <div class="modal-header">
            <h2>Tambah Jadwal 1</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Kode Program</label>
                <select class="custom-select" name="" id="kodeProgram">
                  <option value="0"> -- Pilih Kode Program --  </option>
                    @foreach ($getKodeProgram as $item)
                      <option value="{{$item->kode_p}}">- {{$item->kode_p}} - {{$item->aktivitas}} -</option>                                        
                    @endforeach
                </select>
                <input type="hidden" class="form-control" id="kodeInisial" placeholder="">
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tanggal Mulai Program</label>
                <input type="date" class="form-control" id="tglMulaiProgram" placeholder="">
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tangggal Selesai Program</label>
                <input type="date" class="form-control" id="tglSelesaiProgram" placeholder="">
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Durasi</label>
                <input type="text" class="form-control" id="durasiProgram" placeholder="Durasi Program" readonly>
              </div>
            </div>

            <hr>
            {{-- master_kegiatan --}}
            <div class="form-row" id="next_master_kegiatan">
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Kode Kegiatan</label>
                <select class="custom-select" name="" id="kodeKegiatan">
                  <option value="0" disabled selected hidden> -- Pilih Kode Kegiatan --  </option>
                </select>
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tanggal Mulai Kegiatan</label>
                <input type="date" class="form-control" id="tglMulaiKegiatan" placeholder="">
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tanggal Selesai Kegiatan</label>
                <input type="date" class="form-control" id="tglSelesaiKegiatan" placeholder="">
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Durasi</label>
                <input type="text" class="form-control" id="durasiKegiatan" placeholder="Durasi Kegiatan" readonly>
              </div>
            </div>
            
            <hr>
            {{-- master_tahapan --}}
            <div class="form-row" id="next_master_tahapan">
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Kode Tahapan</label>
                  <input type="text" class="form-control" id="kodeTahapan" placeholder="Kode Tahapan" readonly>
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Tanggal Mulai Tahapan</label>
                  <input type="date" class="form-control" id="tglMulaiTahapan" placeholder="">
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Tanggal Selesai Tahapan</label>
                  <input type="date" class="form-control" id="tglSelesaiTahapan" placeholder="">
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Durasi Tahapan</label>
                  <input type="text" class="form-control" id="durasiTahapan" placeholder="Durasi Tahapan" readonly>
                </div>
              </div> 

              
            <div class="form-row" id="next_master_aktivitas_tahapan">
                <div class="form-group col-lg-12 col-sm-12">
                  <label for="">Tahapan Aktivitas</label>
                  <textarea  class="form-control" id="aktivitasTahapan" placeholder="Tahapan Aktivitas" rows="5"></textarea>
                </div>
              </div> 
          </div>
          <div class="modal-footer" id="footerAction">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button> 
            <button type="button" class="btn btn-success" id="btnSimpanData">Simpan</button>             
          </div>
      </div>
    </div>
  </div>

    {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}

  <div class="modal  fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="viewDetailMaster" aria-hidden="true" id="viewDetailMaster">
    <div class="modal-dialog modal-add">
      <div class="modal-content">
          <div class="modal-header">
            <h2>Tambah Jadwal 1</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Kode Program</label>
                <input type="text" class="form-control" id="viewkodeInisial" placeholder=""readonly>
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tanggal Mulai Program</label>
                <input type="date" class="form-control" id="viewtglMulaiProgram" placeholder=""readonly>
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tangggal Selesai Program</label>
                <input type="date" class="form-control" id="viewtglSelesaiProgram" placeholder=""readonly>
              </div>
              <div class="form-group col-lg-3 col-sm-12">
                <label for="">Durasi</label>
                <input type="text" class="form-control" id="viewdurasiProgram" placeholder="Durasi Program" readonly>
              </div>
            </div>  
          </div>
          <div class="modal-footer" id="footerAction">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>          
          </div>
      </div>
    </div>
  </div>


      
</div>
<script>
  $(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function diffDays(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    $('#tglMulaiProgram,#tglSelesaiProgram').on('focusout', function(){
          date1 = $('#tglMulaiProgram').val();
          date2 = $('#tglSelesaiProgram').val();
          hasilDurasi = diffDays(date1,date2);
          // console.log(hasilDurasi)

          durasiProgram = hasilDurasi +1;    

          if(isNaN(durasiProgram))
          {
            $('#durasiProgram').val('0')
          }
          else
          {
            $('#durasiProgram').val(durasiProgram)            
          }
    })

    
    $('#tglMulaiKegiatan,#tglSelesaiKegiatan').on('focusout', function(){
          date1 = $('#tglMulaiKegiatan').val();
          date2 = $('#tglSelesaiKegiatan').val();
          hasilDurasi = diffDays(date1,date2);

          durasiKegiatan = hasilDurasi +1;    

          if(isNaN(durasiKegiatan))
          {
            $('#durasiKegiatan').val('0')
          }
          else
          {
            $('#durasiKegiatan').val(durasiKegiatan)            
          }
    });

    // Tanggal Tidak Boleh Melebihi Program
    $('#tglMulaiKegiatan,#tglSelesaiKegiatan').on('focusout', function(){
          date1 = $('#tglSelesaiProgram').val();
          date2 = $('#tglSelesaiKegiatan').val();  

          if(date2 > date1)
          {
            alert('Tanggal kegiatan melebihi tanggal program');
            $('#tglSelesaiKegiatan').val('');
            $('#durasiKegiatan').val('');
          }
          else
          {
            // $('#durasiKegiatan').val(durasiKegiatan)            
          }
    });


    
    $('#tglMulaiTahapan,#tglSelesaiTahapan').on('focusout', function(){
          date1 = $('#tglMulaiTahapan').val();
          date2 = $('#tglSelesaiTahapan').val();
          hasilDurasi = diffDays(date1,date2);

          durasiTahapan = hasilDurasi +1;    

          if(isNaN(durasiTahapan))
          {
            $('#durasiTahapan').val('0')
          }
          else
          {
            $('#durasiTahapan').val(durasiTahapan)            
          }
    });
    
    
    $('#tglMulaiTahapan,#tglSelesaiTahapan').on('focusout', function(){
          date1 = $('#tglSelesaiKegiatan').val();  
          date2 = $('#tglSelesaiTahapan').val();
          // console.log(date1)

          if(date2 > date1)
          {
            alert('Tanggal Tahapan melebihi Tanggal Kegiatan');
            $('#tglSelesaiTahapan').val('');
            $('#durasiTahapan').val('');
          }
          else
          {
            // $('#durasiKegiatan').val(durasiKegiatan)            
          }
    });
    
          

      table = $('#table_master').DataTable({
        "processing": true,
        ajax: {
            url: '/kabid/table_manage_kabid',
            type:'get',
        },
      "columns" : [
                { "data" : "no" },
                { "data" : "kodeInisial" },
                { "data" : "aktivitas" },
                { "data" : "tglMulai" },
                { "data" : "tglSelesai" },
                { "data" : "durasiHari" },
                { "data" : "nameGuard" },
            ],
        "columnDefs": [
          {
            "targets": 6,
            render:function(data,type,row,meta){
                return '<button class="btn btn-primary" id="viewMaster" data-toggle="modal" data-target="#viewDetailMaster"><span class="ti-view-grid"></span></button> '+'<button class="btn btn-warning" id="exportMaster"><span class="ti-clipboard"></span></button> '+' <button class="btn btn-danger" id="deleteMaster"><span class="ti-close"></span></button> ';
            }
          },
        ]    
        
      });

      $('#BtntambahMasterSchedule').on('click', function(){
        // kode Program reset
        $('#kodeProgram').val(0);
        $('#tglMulaiProgram').val('');
        $('#tglSelesaiProgram').val('');
        $('#durasiProgram').val('');


        // reset kegeiatan
        $('#next_master_kegiatan').hide();
        $('#next_master_tahapan').hide();
        $('#next_master_aktivitas_tahapan').hide();

        $('#kodeKegiatan').val(0);
        $('#tglMulaiKegiatan').val('');
        $('#tglSelesaiKegiatan').val('');
        $('#durasiKegiatan').val('');

        
        $('#kodeTahapan').val('');
        $('#tglMulaiTahapan').val('');
        $('#tglSelesaiTahapan').val('');
        $('#aktivitasTahapan').val('');
        $('#durasiTahapan').val('');
        
      })

      $('#table_master tbody').on('click','#deleteMaster', function(){
        var data = table.row( $(this).parents('tr') ).data();
        kodeInisial = data.kodeInisial
        // console.log(kodeInisial)
        $.ajax({
          url : '/kabid/deleteMaster/'+kodeInisial,
          type : 'get',
          success:function(data)
          {
            location.reload();
          }
        })
      })
      $('#table_master tbody').on('click','#viewMaster', function(){
        var data = table.row( $(this).parents('tr') ).data();
        kodeInisial = data.kodeInisial
        $('#viewkodeInisial').val('')
        $('#viewtglMulaiProgram').val('')
        $('#viewtglSelesaiProgram').val('')
        $('#viewdurasiProgram').val('')
        // console.log(kodeInisial);
        $.ajax({
          url : '/kabid/viewMaster/'+kodeInisial,
          type : 'get',
          success:function(data)
          {
            result = data.data;

            if(result)
            {
              $('#viewkodeInisial').val(result.kode_i)
              $('#viewtglMulaiProgram').val(result.tgl_mulai)
              $('#viewtglSelesaiProgram').val(result.tgl_selesai)
              $('#viewdurasiProgram').val(result.durasi)
            }
            else
            {
              $('#viewkodeInisial').val('')
              $('#viewtglMulaiProgram').val('')
              $('#viewtglSelesaiProgram').val('')
              $('#viewdurasiProgram').val('')
            }
          }
        })
      })
      
    // Eport Start Here
    $('#table_master tbody').on('click','#exportMaster', function(){
      var data = table.row( $(this).parents('tr') ).data();
      kodeInisial = data.kodeInisial
      // console.log(kodeInisial)

      var url = "/kabid/cetakDataMaster/" + kodeInisial

      window.location = url;
      
    })

      // hide all
      $('#next_master_kegiatan').hide();
      $('#next_master_tahapan').hide();
      $('#next_master_aktivitas_tahapan').hide();
      $('#footerAction').hide();

      $('#tglMulaiProgram,#tglSelesaiProgram').on('change', function(){
      })


      $('#kodeProgram').on('change', function(){ 
        $('#kodeKegiatan').empty();
          $('#tglMulaiProgram').val('').attr('readonly',false);
          $('#tglSelesaiProgram').val('').attr('readonly',false);

        if($('#kodeProgram').val() == 0)
        {
          $('#next_master_kegiatan').hide();
          $('#next_master_tahapan').hide();
          $('#next_master_aktivitas_tahapan').hide();

          $('#kodeProgram').val(0);
          $('#tglMulaiProgram').val('');
          $('#tglSelesaiProgram').val('');
          $('#durasiProgram').val('');

          

          $('#kodeKegiatan').val(0);
          $('#tglMulaiKegiatan').val('');
          $('#tglSelesaiKegiatan').val('');
          $('#durasiKegiatan').val('');

          $('#kodeTahapan').val('');
          $('#tglMulaiTahapan').val('');
          $('#tglSelesaiTahapan').val('');
          $('#aktivitasTahapan').val('');
          $('#durasiTahapan').val('');      
        }
        else
        {
          

          
          kodeParent = $('#kodeProgram').val();

          $('#next_master_kegiatan').show();   
          $.ajax({
              url:'/kabid/getDataProgram/'+kodeParent,
              type:'get',
              success:function(dt)
              {
               data = dt.dt
                $('#tglMulaiProgram').val(data[0].tgl_mulai);
                $('#tglSelesaiProgram').val(data[0].tgl_selesai);
                $('#durasiProgram').val(data[0].durasi);

              }
              
          });

        }
      })

      $('#kodeProgram').on('change', function(){ 
        $('#kodeKegiatan').empty();

        if($('#kodeProgram').val() == 0)
        {
          $('#next_master_kegiatan').hide();
          $('#next_master_tahapan').hide();
          $('#next_master_aktivitas_tahapan').hide();

          $('#kodeProgram').val(0);
          $('#tglMulaiProgram').attr('disabled','disabled');
          $('#tglSelesaiProgram').attr('disabled','disabled');
          $('#durasiProgram').val('');

          

          $('#kodeKegiatan').val(0);
          $('#tglMulaiKegiatan').val('');
          $('#tglSelesaiKegiatan').val('');
          $('#durasiKegiatan').val('');

          $('#kodeTahapan').val('');
          $('#tglMulaiTahapan').val('');
          $('#tglSelesaiTahapan').val('');
          $('#aktivitasTahapan').val('');
          $('#durasiTahapan').val('');      
        }
        else
        {
          

          $('#kodeKegiatan').val(0);
          $('#tglMulaiKegiatan').val('');
          $('#tglSelesaiKegiatan').val('');
          $('#durasiKegiatan').val('');

          $('#kodeTahapan').val('');
          $('#tglMulaiTahapan').val('');
          $('#tglSelesaiTahapan').val('');
          $('#aktivitasTahapan').val('');
          $('#durasiTahapan').val('');      

          
          $('#next_master_tahapan').hide();
          $('#next_master_aktivitas_tahapan').hide();
          
          kodeParent = $('#kodeProgram').val();

          $('#next_master_kegiatan').show();   
          $.ajax({
              url:'/kabid/get_parent_kode_kegiatan/'+kodeParent,
              type:'get',
              success:function(data)
              {
                dt = data.data
                // console.log(dt)
                $('#kodeKegiatan').attr('palceholder','-- Pilih Kode Kegiatan --') 
                $('#kodeKegiatan').append($('<option>', {
                    value: 0,
                    text: '-- Pilih Kode Kegiatan --'
                }));            
                $.each(dt, function (index, item) {
                    $('#kodeKegiatan').append($('<option>', { 
                        value: item.kode_k,
                        text : item.kode_k +' - '+ item.aktivitas,
                        placeholder: {
                          id: '1', // the value of the option
                          text: 'Select an option'
                        },
                    }));
                    
                });
              }
              
          });

        }
      })
      
      $('#kodeKegiatan').on('change', function(){
        kodeKegiatan = $('#kodeKegiatan').val()
        if(kodeKegiatan == 0)
        {
          $('#next_master_tahapan').hide();
          $('#next_master_aktivitas_tahapan').hide();
          $('#footerAction').hide();

          $('#kodeTahapan').val('');
          $('#tglMulaiTahapan').val('');
          $('#tglSelesaiTahapan').val('');  
          $('#aktivitasTahapan').val('');
          $('#tglMulaiKegiatan').val('');
          $('#tglSelesaiKegiatan').val('');
        }
        else
        {
          $('#next_master_tahapan').show();
          $('#next_master_aktivitas_tahapan').show();
          $('#footerAction').show();
          
          $('#tglMulaiTahapan').val('');
          $('#tglSelesaiTahapan').val('');
          $('#aktivitasTahapan').val('');
          $('#durasiTahapan').val('');    
          kodeKegiatan = $('#kodeKegiatan').val();
          $.ajax({
            url : '/kabid/get_count_kegiatan/'+kodeKegiatan,
            type : 'get',
            success:function(data)
            {
              dt =  data.data;

              // console.log(dt);             

              $('#kodeTahapan').val('00'+dt);
            }
          })
          
          $.ajax({
            url : '/kabid/get_date_kegiatan/'+kodeKegiatan,
            type : 'get',
            success:function(data)
            {
              dt =  data.data;      
              $('#tglMulaiKegiatan').val(dt.tgl_mulai);
              $('#tglSelesaiKegiatan').val(dt.tgl_selesai);     

              // $('#kodeTahapan').val('00'+dt);
            }
          })

        }
      });
      
      $('#btnSimpanData').on('click', function(){
        
        kodeProgram = $('#kodeProgram').val();
        tglMProgram = $('#tglMulaiProgram').val();
        tglSProgram = $('#tglSelesaiProgram').val();
        durasiProgram = $('#durasiProgram').val();

        

        kodeKegiatan = $('#kodeKegiatan').val();
        tglMKegiatan = $('#tglMulaiKegiatan').val();
        tglSKegiatan = $('#tglSelesaiKegiatan').val();
        durasiKegiatan = $('#durasiKegiatan').val();

        kodeTahapan = $('#kodeTahapan').val();
        tglMTahapan = $('#tglMulaiTahapan').val();
        tglSTahapan = $('#tglSelesaiTahapan').val();
        aktivitasTahapan = $('#aktivitasTahapan').val();
        durasiTahapan = $('#durasiTahapan').val(); 

        $.ajax({
          url : '/kabid/postTahapan',
          type : 'post',
          data : {kodeProgram:kodeProgram,
                  tglMProgram:tglMProgram,
                  tglSProgram:tglSProgram,
                  durasiProgram:durasiProgram,
                  kodeKegiatan:kodeKegiatan,
                  tglMKegiatan:tglMKegiatan,
                  tglSKegiatan:tglSKegiatan,
                  durasiKegiatan:durasiKegiatan,
                  kodeTahapan:kodeTahapan,
                  tglMTahapan:tglMTahapan,
                  tglSTahapan:tglSTahapan,
                  aktivitasTahapan:aktivitasTahapan,
                  durasiTahapan:durasiTahapan},
          success:function(data)
          {
            location.reload()
          }
        })



      })
      


    });
</script>

@endsection