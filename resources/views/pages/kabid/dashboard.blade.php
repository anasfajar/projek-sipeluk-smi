@extends('layouts.kabid.master')

@section('title','Dashboard Kabid')
<style>
.modal-add {
    max-width:70% !important;
    margin:0 auto;
    /*add what you want here*/
}
</style>

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
        <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Program</div>
                          <div class="stat-digit">{{$count_program}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-lg-6 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Kegiatan</div>
                          <div class="stat-digit">{{$count_kegiatan}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      </div>



  {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}


      
</div>
<script>
  $(document).ready( function () {
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

      // $('#table_master').DataTable({
      //   "processing": true,
      //   ajax: {
      //       url: '/kabid/table_manage_kabid',
      //       type:'get',
      //   },
      // "columns" : [
      //           { "data" : "no" },
      //           { "data" : "kodeProgram" },
      //           { "data" : "kodeKegiatan" },
      //           { "data" : "kodeTahapan" },
      //           { "data" : "aktivitas" },
      //           { "data" : "tglMulai" },
      //           { "data" : "tglSelesai" },
      //           { "data" : "durasiHari" },
      //       ],
      //   "columnDefs": [
      //     {
      //       "targets": 8,
      //       data: 'tipeKode',
      //       render:function(data,type,row,meta){
      //         if(data == 1)
      //         {
      //           return '<button class="btn btn-primary" data-toggle="modal" data-target="#myEdit" id="detil_master"><span class="ti-list"></span></button> '
      //                 // +' <button class="btn btn-da nger" data-toggle="modal" data-target="#" id=""><span class="ti-user"></span></button>'
      //                 // +' <button class="btn btn-success" data-toggle="modal" data-target="#myAddKegiatan" id="addKegiatan"><span class="ti-plus"></span> Kegiatan</button>';
      //         }
      //         else if(data == 2)
      //         {
      //           return '<button class="btn btn-primary" data-toggle="modal" data-target="#myEditKegiatan" id="detil_kegiatan"><span class="ti-list"></span></button> '
      //                 +' <button class="btn btn-success" data-toggle="modal" data-target="#myAddKegiatan" id="addKegiatan"><span class="ti-plus"></span> Tahapan Kegiatan</button>';            
      //         }
      //         else
      //         {

      //         }
      //       }
      //     },
      //   ]    
        
      // });

    });
</script>

@endsection