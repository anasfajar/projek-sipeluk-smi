@extends('layouts.admin.master')

@section('title','Dashboard Admin')

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                          <li></li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>


      <div class="content mt-1">
          @if (session('status'))
          <div class="col-sm-12">
              <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Success</span> @session('status')
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          </div>
          @endif

          
          <div class="col-sm-12">
            
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header">
                          <strong class="card-title">Update User <small><span class="badge badge-warning float-right mt-1">Edit Data</span></small></strong>
                      </div>
                      <div class="card-body">
                          <form method="POST" action="{{ route('admin.manage_edit.post') }}">
                              @csrf
              
                              <div class="form-group row">
                                  <input type="hidden" name="id" value="{{$get_user->id}}">
                                  <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
              
                                  <div class="col-md-6">
                                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $get_user->name }}" required autocomplete="name" autofocus>
              
                                      @error('name')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                  </div>
                              </div>
              
                              <div class="form-group row">
                                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
              
                                  <div class="col-md-6">
                                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $get_user->email}}" required autocomplete="email">
              
                                      @error('email')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                  </div>
                              </div>
              
                              <div class="form-group row">
                                  <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                               
                                  <div class="col-md-6">
                                    @if($get_user->role == 'admin')
                                      <select name="role" class="form-control"  >
                                          <option value="admin" selected>Admin</option>
                                          <option value="kabid">Kepala Bidang</option>
                                          <option value="kasubid" >Kepala Sub Bidang</option>
                                      </select> 
                                    @elseif($get_user->role == 'kabid')
                                      <select name="role" class="form-control" >
                                          <option value="admin">Admin</option>
                                          <option value="kabid" selected>Kepala Bidang</option>
                                          <option value="kasubid">Kepala Sub Bidang</option>
                                      </select> 
                                    @elseif($get_user->role == 'kasubid')
                                      <select name="role" class="form-control" >
                                          <option value="admin">Admin</option>
                                          <option value="kabid">Kepala Bidang</option>
                                          <option value="kasubid" selected >Kepala Sub Bidang</option>
                                      </select> 
                                    @endif
                                  </div>
                              </div>
                              
                              <div class="form-group row mb-0">
                                  <div class="col-md-6 offset-md-4">
                                      <button type="submit" class="btn btn-primary">
                                          {{ __('Update User') }}
                                      </button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>

      </div>
</div>

{{-- Modal Register --}}
{{-- End Modal --}}
@endsection