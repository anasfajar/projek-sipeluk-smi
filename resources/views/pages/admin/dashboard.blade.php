@extends('layouts.admin.master')

@section('title','Dashboard Admin')

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
    
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">User</div>
                            <div class="stat-digit">{{!empty($count_user)?$count_user:'0'}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Total Program</div>
                            <div class="stat-digit">{{!empty($count_program)?$count_program:'0'}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Total Kegiatan</div>
                            <div class="stat-digit">{{!empty($count_kegiatan)?$count_kegiatan:'0'}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <strong class="card-title" style="color:white">Tabel Create Master Data</strong>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#myModal" id="add_program"> Tambah Program</button>
                </div>
                <div class="card-body">
                    <table class="table table-striped"  id="table_master" >
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode Program </th>
                                <th scope="col">Kode Kegiatan </th>
                                <th scope="col">Kode Inisial </th>
                                <th scope="col">Nama Program/Kegiatan/ Tahapan/Langkah</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
      </div>


      <!-- Modal program -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Program</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="form-row">
                <div class="form-group col-lg-2 col-sm-12">
                  <label for="">Master Program</label>
                  <input type="hidden" id="m_kode">
                  <input type="text" class="form-control" id="kode" placeholder="Kode">
                </div>
                <div class="form-group col-lg-10 col-sm-12">
                  <label for="">Nama Program</label>
                  <input type="text" class="form-control" id="aktivitas" placeholder="Nama Program">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" id="submit_data">Submit</button>
            </div>
          </div>

        </div>
      </div>

      {{-- Edit program --}}
      <div id="myEdit" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
  
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Edit Program</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-lg-2 col-sm-12">
                    <label for="">Master Program</label>
                    <input type="hidden" id="edit_master_kode">
                    <input type="hidden" id="edit_kode_old">
                    <input type="text" class="form-control" id="edit_kode" placeholder="Kode">
                  </div>
                  <div class="form-group col-lg-10 col-sm-12">
                    <label for="">Nama Program</label>
                    <input type="text" class="form-control" id="e_aktivitas" placeholder="Nama Program">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit_data_edit">Submit</button>
              </div>
            </div>
  
          </div>
        </div>

        
      <!-- Modal Kegiatan -->
      <div id="myAddKegiatan" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
  
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-lg-2 col-sm-12">
                    <label for="">Kode Program</label>
                    <input type="hidden" id="kode_m">
                    <input type="text" class="form-control" id="kode_p" placeholder="Kode" readonly>
                  </div>
                  <div class="form-group col-lg-2 col-sm-12">
                    <label for="">Kode Kegiatan</label>
                    <input type="text" class="form-control" id="kode_k" placeholder="">
                  </div>
                  <div class="form-group col-lg-8 col-sm-12">
                    <label for="">Nama Kegiatan</label>
                    <input type="text" class="form-control" id="aktivitas_k" placeholder="Nama Kegiatan">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit_data_kegiatan">Submit</button>
              </div>
            </div>
  
          </div>
        </div>

            
      <!-- Modal edit Kegiatan -->
      <div id="myEditKegiatan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Kegiatan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="form-row">
                <div class="form-group col-lg-2 col-sm-12">
                  <label for="">Kode Program</label>
                  <input type="hidden" id="kode_m_edit">
                  <input type="text" class="form-control" id="kode_p_edit" placeholder="Kode" readonly>
                </div>
                <div class="form-group col-lg-2 col-sm-12">
                  <label for="">Kode Kegiatan</label>
                  <input type="text" class="form-control" id="kode_k_edit" placeholder="">
                </div>
                <div class="form-group col-lg-8 col-sm-12">
                  <label for="">Nama Program/Kegiatan/ Tahapan/Langkah</label>
                  <input type="text" class="form-control" id="aktivitas_k_edit" placeholder="Nama Program/Kegiatan/ Tahapan/Langkah">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" id="submit_edit_kegiatan">Submit</button>
            </div>
          </div>

        </div>
      </div>
      
</div>
<script>
  $(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    table = $('#table_master').DataTable({
      
      ajax: {
          url: '/admin/table_manage_kegiatan',
          type:'get',
      },
      "columns" : [
                { "data" : "no" },
                { "data" : "p_kode" },
                { "data" : "k_kode" },
                { "data" : "kodeInisial" },
                { "data" : "aktivitas" },
                { "data" : "isActive" },
                { "data" : "t_kode" },
            ],
      "columnDefs": [
          // {
          //   "targets" :
          // },
          {
            "targets": 6,
            "data": 't_kode',
            render:function(data,type,row,meta){
              console.log(row.isActive)
              if(data == 1)
              {
                return '<button class="btn btn-primary" data-toggle="modal" data-target="#myEdit" id="detil_master" ><span class="ti-list"></span></button> '+
                      // +' <button class="btn btn-da nger" data-toggle="modal" data-target="#" id=""><span class="ti-user"></span></button>'
                      ' <button class="btn btn-success" data-toggle="modal" data-target="#myAddKegiatan" id="addKegiatan"><span class="ti-plus"></span> Kegiatan</button> '+
                      '<button class="btn btn-danger"  id="cetakDataProgram"><span class="ti-files"></span></button> ';
              }
              else if(data == 2)
              {
                return '<button class="btn btn-warning" data-toggle="modal" data-target="#" '+(row.isActive == 'Aktif' ? 'id="editStatusNonaktif"' : 'id="editStatus"')+'><span class="ti-check-box"></span></button> '+
                       '<button class="btn btn-primary" data-toggle="modal" data-target="#myEditKegiatan" id="detil_kegiatan"><span class="ti-list"></span></button> '+
                       '<button class="btn btn-danger"  id="cetakDataKegiatan"><span class="ti-files"></span></button> ';            
              }
              else if(data == 3)
              {
                return '<button class="btn btn-warning" data-toggle="modal" data-target="#" '+(row.isActive == 'Aktif' ? 'id="editStatusNonaktif"' : 'id="editStatus"')+'><span class="ti-check-box"></span></button> '+
                       '<button class="btn btn-danger"  id="cetakDataTahapan"><span class="ti-files"></span></button> ';
              }
              else
              {
                return '<button class="btn btn-warning" data-toggle="modal" data-target="#" '+(row.isActive == 'Aktif' ? 'id="editStatusNonaktif"' : 'id="editStatus"')+'><span class="ti-check-box"></span></button>';
              }
            }
          },
        ]      
    });

    // change Status Aktif
    $('#table_master tbody').on('click','#editStatus', function(){
      var data = table.row( $(this).parents('tr') ).data();
        let isActive = data.isActive;
        let kodeInisial = data.kodeInisial;
      
          $.ajax({
            url : '/admin/change_manage_aktif/'+kodeInisial,
            method :'get',
            // responseType: 'blob',
            success:function(data)
            {

              kode = data.data

              if(kode == 'Success')
              {
                alert('Data Telah Aktif');
                window.location.reload();
              }

            }
          })

    })
    
    $('#table_master tbody').on('click','#editStatusNonaktif', function(){
      var data = table.row( $(this).parents('tr') ).data();
        let isActive = data.isActive;
        let kodeInisial = data.kodeInisial;
        console.log(kodeInisial)
      
          $.ajax({
            url : '/admin/change_manage_nonaktif/'+kodeInisial,
            method :'get',
            // responseType: 'blob',
            success:function(data)
            {

              kode = data.data

                if(kode == 'Success')
                {
                  alert('Data Telah Nonaktif');
                  window.location.reload();
                }

            }
          })

    })


    // Eport Start Here
    $('#table_master tbody').on('click','#cetakDataProgram', function(){
      var data = table.row( $(this).parents('tr') ).data();
      kodeInisial = data.p_kode
      console.log(kodeInisial)

      var url = "/admin/cetakDataProgram/" + kodeInisial

      window.location = url;
      
      // $.ajax({
      //   url : '/admin/cetakDataProgram/'+kodeInisial,
      //   method :'get',
      //   responseType: 'blob',
      //   success:function(data)
      //   {

      //   }
      // })
    })
    $('#table_master tbody').on('click','#cetakDataKegiatan', function(){
      var data = table.row( $(this).parents('tr') ).data();
      kodeInisial = data.kodeInisial

      var url = "/admin/cetakDataKegiatan/" + kodeInisial

      window.location = url;
      
      // $.ajax({
      //   url : '/admin/cetakDataKegiatan/'+kodeInisial,
      //   method :'get',
      //   responseType: 'blob',
      //   success:function(data)
      //   {

      //   }
      // })
    })
    $('#table_master tbody').on('click','#cetakDataTahapan', function(){
      var data = table.row( $(this).parents('tr') ).data();
      kodeInisial = data.kodeInisial

      var url = "/admin/cetakDataTahapan/" + kodeInisial

      window.location = url;
      
      // $.ajax({
      //   url : '/admin/cetakDataTahapan/'+kodeInisial,
      //   method :'get',
      //   responseType: 'blob',
      //   success:function(data)
      //   {

      //   }
      // })
    })

    // Export End Here  

    $('#table_master tbody').on('click','#detil_master', function(){
      
      
      $('#edit_master_kode').val('');
      $('#edit_kode_old').val('');
      $('#edit_kode').val('');
      $('#e_aktivitas').val('');
      $('#submit_data_edit').prop( "disabled", false )

      var data = table.row( $(this).parents('tr') ).data();

      id = data.kode;
      aktivitas = data.aktivitas;
      pkode = data.p_kode;
      tkode = data.t_kode
      // console.log(data)
      
      // $('#e_m_kode').val(data.kodeProgram);
      $('#edit_master_kode').val(data.kodeProgram);
      $('#edit_kode_old').val(data.kodeProgram);
      $('#edit_kode').val(data.kodeProgram);
      $('#e_aktivitas').val(aktivitas);

    });
    
    $('#table_master tbody').on('click','#detil_kegiatan', function(){

      $('#kode_m_edit').val('');
      $('#kode_p_edit').val('');
      $('#kode_k_edit').val('');
      $('#aktivitas_k_edit').val('');

      var data = table.row( $(this).parents('tr') ).data();   
      console.log(data)
      // id
      $('#kode_m_edit').val(data.k_kode);
      $('#kode_p_edit').val(data.p_kode);
      $('#kode_k_edit').val(data.k_kode);
      $('#aktivitas_k_edit').val(data.aktivitas);

    });



    $('#add_program').on('click', function(){
      m_kode = $('#m_kode').val('0');
      kode = $('#kode').val('');
      aktivitas = $('#aktivitas').val('');
    })

    // add 

    $('#submit_data').on('click', function(){
      m_kode = $('#m_kode').val();
      kode = $('#kode').val();
      aktivitas = $('#aktivitas').val();

      if(kode == '' && aktivitas == '' )
      {
        alert('Ada Data Kosong !');
      }
      else
      {
        $.ajax({
          url : '/admin/post_manage_kegiatan',
          type : 'post',
          data : {m_kode:m_kode,kode:kode,aktivitas:aktivitas},
          success:function(data)
          {
            if(data.data == 'sukses')
            {
              alert('Data sudah di Tambah');
              window.location.reload();              
            }
            else
            {
              alert('Data Program Sudah Ada');

            }            
          }
        });
      }
    });



    $('#table_master tbody').on('click','#addKegiatan', function(){
      
      
      $('#kode_m').val('');
      $('#kode_p').val('');
      $('#kode_k').val('');
      $('#aktivitas_k').val('');
      $('#submit_data_edit').prop( "disabled", false )

      var data = table.row( $(this).parents('tr') ).data();

      id = data.kode;
      aktivitas = data.aktivitas;
      pkode = data.p_kode;
      tkode = data.t_kode

      $('#kode_m').val(data.kodeProgram);
      $('#kode_p').val(data.kodeProgram);

    });


    // edit

    $('#submit_data_edit').on('click', function(){
      m_kode = $('#edit_master_kode').val();
      b_kode = $('#edit_kode_old').val();
      kode = $('#edit_kode').val();
      aktivitas = $('#e_aktivitas').val();
      

      if(kode == '' && aktivitas == '' && tgl_mulai == '')
      {
        alert('Ada Data Kosong !');
      }
      else
      {
        $.ajax({
          url : '/admin/edit_manage_kegiatan',
          type : 'post',
          data : {m_kode:m_kode,b_kode:b_kode,kode:kode,aktivitas:aktivitas},
          success:function(data)
          {
              // console.log(data.data)

              if(data.data == 'sukses')
              {
                alert('Data sudah di update');
                window.location.reload();
              }
              else
              {

              }                     
          }
        });
      }
    });


// Manage Kegiatan ================================================================== // 
    

    $('#submit_data_kegiatan').on('click', function(){
      kode_p = $('#kode_p').val();
      kode_k = $('#kode_k').val();
      aktivitas_k = $('#aktivitas_k').val();
      
      if(kode_p == '' && kode_k == '' && aktivitas_k == '')
      {
        alert('Ada data yang kosong')
      }
      else
      {
          $.ajax({
            url:'/admin/post_add_kegiatan',
            type: 'post',
            data: {kode_p:kode_p,kode_k:kode_k,aktivitas_k:aktivitas_k},
            success:function(dt)
            {
              if(dt.dt == 'success')
              {
                alert('Data Kegiatan sudah di tambah.')
                window.location.reload();
              }
              // else
              // {
              //   alert('Kode Program sudah ada');
              //   // window.location.reload();

              // }
              
            }
          })

      }

      
    })

    
    $('#submit_edit_kegiatan').on('click', function(){
      kode_m = $('#kode_m_edit').val();
      kode_p = $('#kode_p_edit').val();
      kode_k = $('#kode_k_edit').val();
      aktivitas_k = $('#aktivitas_k_edit').val();
      
      if(kode_m == '' && kode_p == '' && kode_k == '' && aktivitas_k == '')
      {
        alert('Ada data yang kosong')
      }
      else
      {
          $.ajax({
            url:'/admin/post_edit_kegiatan',
            type: 'post',
            data: {kode_m:kode_m,kode_p:kode_p,kode_k:kode_k,aktivitas_k:aktivitas_k},
            success:function(dt)
            {
              if(dt.dt == 'success')
              {
                alert('Data Kegiatan sudah di tambah.')
                window.location.reload();
              }
              else
              {
                alert('Kode Program sudah ada');
                // window.location.reload();

              }
              
            }
          })

      }

      
    })







  });
</script>

@endsection