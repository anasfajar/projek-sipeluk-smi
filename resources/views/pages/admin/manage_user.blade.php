@extends('layouts.admin.master')

@section('title','Dashboard Admin')

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.admin.header_master')    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                          <li></li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>


      <div class="content mt-1">
          @if (session('status'))
          <div class="col-sm-12">
              <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Success</span> {{session('status')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          </div>
          @elseif (session('update'))
          <div class="col-sm-12">
              <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-primary">Update</span> {{session('update')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          </div>
          @elseif (session('delete'))
          <div class="col-sm-12">
              <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          </div>
          @elseif (session('repassword'))
          <div class="col-sm-12">
              <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-danger">Password</span> {{session('repassword')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          </div>
          @endif

          
          <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Stripped Table</strong>
                    </div>
                    <div class="card-body">
                        <button type="button" class="btn btn-success btn-lg m-1" data-toggle="modal" data-target=".modal-register">Tambah User</button>
                        <table class="table table-striped text-center " >
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Level</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            @foreach($data_user as $item)
                                <tbody>
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{ucfirst($item->name)}}</td>
                                        <td>{{ucfirst($item->email)}}</td>
                                            @if($item->role === 'admin')
                                                <td>Admin</td>
                                            @elseif($item->role === 'kabid')
                                                <td>Kepala Bidang</td>
                                            @elseif($item->role === 'kasubid')
                                                <td>Kepala Sub Bidang</td>
                                            @endif
                                        <td>
                                            <a href="" class="btn btn-warning btn-sm" data-toggle="modal" data-target=".modal-edit-{{$item->id}}"> Edit </a> 
                                            
                                            <div class="modal fade modal-edit-{{$item->id}} p-0" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$item->id}}" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="modal-edit-{{$item->id}}">Tambah User</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="POST" action="{{ route('admin.manage_edit.post') }}">
                                                            @csrf

                                                            <div class="form-group row">
                                                                <input type="hidden" name="id" value="{{$item->id}}">
                                                                <label for="name" class="col-md-4 col-form-label text-left">{{ __('Nama User') }}</label>

                                                                <div class="col-md-6">
                                                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name }}" required autocomplete="name" autofocus>

                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="email" class="col-md-4 col-form-label text-left">{{ __('E-Mail Address') }}</label>

                                                                <div class="col-md-6">
                                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $item->email }}" required autocomplete="email">

                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="role" class="col-md-4 col-form-label text-left">Role</label>
                                                            
                                                                <div class="col-md-6">
                                                                    @if($item->role == 'admin')
                                                                    <select name="role" class="form-control"  >
                                                                        <option value="admin" selected>Admin</option>
                                                                        <option value="kabid">Kepala Bidang</option>
                                                                        <option value="kasubid" >Kepala Sub Bidang</option>
                                                                    </select> 
                                                                    @elseif($item->role == 'kabid')
                                                                    <select name="role" class="form-control" >
                                                                        <option value="admin">Admin</option>
                                                                        <option value="kabid" selected>Kepala Bidang</option>
                                                                        <option value="kasubid">Kepala Sub Bidang</option>
                                                                    </select> 
                                                                    @elseif($item->role == 'kasubid')
                                                                    <select name="role" class="form-control" >
                                                                        <option value="admin">Admin</option>
                                                                        <option value="kabid">Kepala Bidang</option>
                                                                        <option value="kasubid" selected >Kepala Sub Bidang</option>
                                                                    </select> 
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            {{--  Data user here  --}}
                                                            
                                                            <div class="form-group row">
                                                                <label for="nip" class="col-md-4 col-form-label text-left">{{ __('NIP') }}</label>
                                                                <div class="col-md-6">
                                                                    <input id="" type="text" class="form-control " value="{{ !empty($item->nip_pegawai)?$item->nip_pegawai :'' }}" name="nip" required >
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="nama_pegawai" class="col-md-4 col-form-label text-left">{{ __('Nama Lengkap Pegawai') }}</label>
                                                                <div class="col-md-6">
                                                                    <input id="" type="text" class="form-control " name="nama_pegawai" value="{{ !empty($item->nama_pegawai)?$item->nama_pegawai :'' }}" required >
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="jabatan" class="col-md-4 col-form-label text-left">Jabatan Pegawai</label>
                                                            
                                                                <div class="col-md-6">
                                                                        @if($item->jabatan_pegawai == 'admin')
                                                                        <select name="jabatan" class="form-control"  >
                                                                            <option value="Admin" selected>Admin</option>
                                                                            <option value="Kepala Bidang">Kepala Bidang</option>
                                                                            <option value="Kepala Sub Bidang">Kepala Sub Bidang</option>
                                                                        </select> 
                                                                        @elseif($item->jabatan_pegawai == 'Kepala Bidang')
                                                                        <select name="jabatan" class="form-control" >
                                                                            <option value="Admin">Admin</option>
                                                                            <option value="Kepala Bidang" selected>Kepala Bidang</option>
                                                                            <option value="Kepala Sub Bidang">Kepala Sub Bidang</option>
                                                                        </select> 
                                                                        @elseif($item->jabatan_pegawai == 'Kepala Sub Bidang')
                                                                        <select name="jabatan" class="form-control" >
                                                                            <option value="Admin">Admin</option>
                                                                            <option value="Kepala Bidang">Kepala Bidang</option>
                                                                            <option value="Kepala Sub Bidang" selected>Kepala Sub Bidang</option>
                                                                        </select> 
                                                                        @else
                                                                            <select name="jabatan" class="form-control" >
                                                                                <option value="Admin">Admin</option>
                                                                                <option value="Kepala Bidang">Kepala Bidang</option>
                                                                                <option value="Kepala Sub Bidang">Kepala Sub Bidang</option>
                                                                            </select> 
                                                                        @endif
                                                                </div>
                                                            </div>

                                                            
                                                            <div class="form-group row">
                                                                <label for="keterangan" class="col-md-4 col-form-label text-left">{{ __('Keterangan') }}</label>
                                                                <div class="col-md-6">
                                                                <textarea name="keterangan" class="form-control" id="" rows="3">{{!empty($item->keterangan_pegawai)?$item->keterangan_pegawai:''}}</textarea>
                                                                </div>
                                                            </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">
                                                                {{ __('Register') }}
                                                            </button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        {{-- <button type="button" class="btn btn-primary">Send message</button> --}}
                                                    </div>
                                                    </form> 
                                                </div>
                                                </div>
                                            </div>
                                            </div>




                                            <a href="" class="btn btn-success btn-sm" data-toggle="modal" data-target=".modal-password-{{$item->id}}">Ubah Password</a>
                                                  <div class="modal fade modal-password-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-password" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="modal-password-{{$item->id}}">Ubah Password User</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                              <form method="POST" action="{{ route('admin.manage_user.repassword') }}">
                                                                  @csrf
                                                                  <div class="form-group row">
                                                  
                                                                      <div class="col-12">
                                                                          <input type="hidden" name="id" value="{{$item->id}}">
                                                                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                                                  
                                                                          @error('password')
                                                                              <span class="invalid-feedback" role="alert">
                                                                                  <strong>{{ $message }}</strong>
                                                                              </span>
                                                                          @enderror
                                                                      </div>
                                                                  </div>
                                                                  
                                                                  <div class="form-group row mb-0">
                                                                      <div class="col-12">
                                                                          <button type="submit" class="btn btn-primary btn-block">
                                                                              {{ __('Ubah Password') }}
                                                                          </button>
                                                                      </div>
                                                                  </div>
                                                              </form>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                            <a href="{{route('admin.manage_delete_user',['id' => $item->id])}}" class="btn btn-danger btn-sm"> Hapus </a>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

      </div>
</div>

{{-- Modal Register --}}
<div class="modal fade modal-register p-0" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-register">Tambah User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{ route('admin.manage_user.post') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-left">{{ __('Nama User') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-left">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-left">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role" class="col-md-4 col-form-label text-left">Role</label>
                 
                    <div class="col-md-6">
                        <select name="role" class="form-control" >
                            <option value="admin">Admin</option>
                            <option value="kabid">Kepala Bidang</option>
                            <option value="kasubid">Kepala Sub Bidang</option>
                        </select> 
                    </div>
                </div>

                <hr>

                {{--  Data user here  --}}
                
                <div class="form-group row">
                    <label for="nip" class="col-md-4 col-form-label text-left">{{ __('NIP') }}</label>
                    <div class="col-md-6">
                        <input id="" type="text" class="form-control " name="nip" required >
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="nama_pegawai" class="col-md-4 col-form-label text-left">{{ __('Nama Lengkap Pegawai') }}</label>
                    <div class="col-md-6">
                        <input id="" type="text" class="form-control " name="nama_pegawai" required >
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="jabatan" class="col-md-4 col-form-label text-left">Jabatan Pegawai</label>
                 
                    <div class="col-md-6">
                        <select name="jabatan" class="form-control" >
                            <option value="Admin">Admin</option>
                            <option value="Kepala Bidang">Kepala Bidang</option>
                            <option value="Kepala Sub Bidang">Kepala Sub Bidang</option>
                        </select> 
                    </div>
                </div>

                
                <div class="form-group row">
                    <label for="keterangan" class="col-md-4 col-form-label text-left">{{ __('Keterangan') }}</label>
                    <div class="col-md-6">
                      <textarea name="keterangan" class="form-control" id="" rows="3"></textarea>
                    </div>
                </div>

        </div>
          <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                </button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary">Send message</button> --}}
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

{{-- End Modal --}}
@endsection