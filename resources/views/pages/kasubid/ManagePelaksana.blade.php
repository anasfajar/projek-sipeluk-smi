@extends('layouts.kasubid.master')

@section('title','Dashboard Kasubid')
<style>
.modal-add {
    max-width:70% !important;
    margin:0 auto;
    /*add what you want here*/
}
</style>

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.kasubid.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
        <div class="col-lg-12 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Tanggung Jawab Kegiatan</div>
                          <div class="stat-digit">{{$getCount?$getCount:'0'}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <strong class="card-title" style="color:white">Tabel Pelaksanaan Kegiatan</strong>
                    {{-- <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#myModal" id="add_program"></button> --}}
                </div>
                <div class="card-body">
                    <table class="table table-striped"  id="table_master" >
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Tahapan Kegiatan</th>
                                <th scope="col">Tanggal Mulai</th>
                                <th scope="col">Tanggal Selesai</th>
                                <th scope="col">Durasi</th>
                                <th scope="col">persentase</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>

      </div>

      {{-- Tambah Anak Tahapan Kegiatan  --}}
      <div id="saveDataLangkah" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Input Pelaksanaan Kegiatan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form action="/kasubid/postPelaksana" enctype="multipart/form-data" method="POST">
                @csrf
                  <div class="form-row">
                    <div class="form-group col-lg-4 col-sm-12">
                      <label for="">Kode Program</label>
                      <input type="text" class="form-control" id="kodeInisial" placeholder="Kode" name="kodeInisial" readonly>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                      <label for="">Tanggal Mulai Kegiatan</label>
                      <input type="date" class="form-control" id="tglMulaiTahapan" placeholder="Kode" readonly>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                    <label for="">Tanggal Selesai Kegiatan</label>
                      <input type="date" class="form-control" id="tglSelesaiTahapan" placeholder="" readonly>
                    </div>
                    {{-- <div class="form-group col-lg-8 col-sm-12">
                      <label for="">Program Kegiatan</label>
                      <input type="text" class="form-control" id="aktivitas_k_edit" placeholder="Program Kegiatan">
                    </div> --}}
                  </div>
                  <div class="form-row">
                    <div class="form-group col-lg-6 col-sm-12">
                      <label for="">Tanggal Mulai Langkah</label>
                      <input type="date" class="form-control" name="tglMulaiPelaksana" id="tglMulaiPelaksana" placeholder="Kode" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                      <label for="">Status Keterangan</label>
                        <select class="form-control" id="statusPelaksana" name="statusPelaksana" required>
                          <option value="">Pilih Status</option>
                          <option value="1">On Proses</option>
                          <option value="2">On Time</option>
                          <option value="3">Over Time</option>
                        </select>
                    </div>
                  </div>
                  
                  <div class="form-row">
                      <div class="form-group col-lg-12 col-sm-12">
                          <label for="customRange2">Persentasi</label>
                          <input type="range" class="custom-range" name="rangeData" min="0" max="100" id="rangeData">
                      </div>
                    </div>
                  <div class="form-row">
                    <div class="form-group col-lg-12 col-sm-12">
                      <label for="">Dokumen Bukti Pelaksana</label>
                      <input type="file" class="form-control" name="dokumenBukti" placeholder="Kode" required>
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="form-group col-lg-12 col-sm-12">
                      <label for="">OutPut</label>
                      <textarea type="text" class="form-control" rows="5" id="aktivitasLangkah" name="aktivitasLangkah" placeholder="OutPut" required ></textarea>
                    </div>
                  </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success" id="submitDataLangkah">Submit</button>
            </div>
          </form>
          </div>

        </div>
      </div>

      <div id="viewDataLangkah" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">View Kegiatan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                  <div class="form-row">
                    <div class="form-group col-lg-4 col-sm-12">
                      <label for="">Kode Program</label>
                      <input type="text" class="form-control" id="viewkodeInisial" placeholder="Kode"  readonly>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                      <label for="">Tanggal Mulai Kegiatan</label>
                      <input type="date" class="form-control" id="viewtglMulaiTahapan" placeholder="Kode" readonly>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                    <label for="">Tanggal Selesai Kegiatan</label>
                      <input type="date" class="form-control" id="viewtglSelesaiTahapan" placeholder="" readonly>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-lg-6 col-sm-12">
                      <label for="">Tanggal Mulai Langkah</label>
                      <input type="date" class="form-control" id="viewtglMulaiPelaksana" placeholder="Kode" required readonly>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                      <label for="">Status Keterangan</label>
                        <select class="form-control" id="viewstatusPelaksana" required readonly>
                          <option value="">Pilih Status</option>
                          <option value="1">On Proses</option>
                          <option value="2">On Time</option>
                          <option value="3">Over Time</option>
                        </select>
                    </div>
                  </div>
                  
                  <div class="form-row">
                      <div class="form-group col-lg-12 col-sm-12">
                          <label for="customRange2">Persentasi</label>
                          <input type="range" class="custom-range" min="0" max="100" id="viewrangeData"readonly>
                      </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="form-group col-lg-12 col-sm-12">
                      <label for="">OutPut</label>
                      <textarea type="text" class="form-control" rows="5" id="viewaktivitasLangkah" placeholder="OutPut" required readonly ></textarea>
                    </div>
                  </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>


  {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}


      
</div>
<script>
  $(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    function diffDays(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
    
    table = $('#table_master').DataTable({
      
      ajax: {
          url: '/kasubid/getTablePelaksana',
          type:'get',
      },
      "columns" : [
                { "data" : "no" },
                { "data" : "kodeInisial" },
                { "data" : "aktivitas" },
                { "data" : "tglMulai" },
                { "data" : "tglSelesai" },
                { "data" : "durasiHari" },
                { "data" : "persentase" },
            ],
        "columnDefs": [
          {
            "targets": 6,
            "visible": false,
          },
          {
            "targets": 7,
            data: 'tipeKode',
            render:function(data,type,row,meta){
                return ' <button class="btn btn-primary"'+(row.persentase?'id="btnViewDataLangkah" data-toggle="modal" data-target="#viewDataLangkah"':'id="btnDataLangkah" data-toggle="modal" data-target="#saveDataLangkah"')+' ><span '+(row.persentase?'class="ti-notepad"':'class="ti-pencil-alt"')+'></span></button> ' +' <button class="btn btn-success" '+(row.persentase?'':'hidden')+' id="cetakPelaksana"><span class="ti-clipboard"></span></button> ';
            }
          },
        ]      
    });
    
    console.log(table)

    $('#table_master tbody').on('click','#btnDataLangkah', function(){
      var data = table.row($(this).parents('tr')).data();
      // console.log(data)
      // alert('test')
      $('#kodeInisial').val(data.kodeInisial)
      $('#tglMulaiTahapan').val(data.tglMulai)
      date1 = $('#tglSelesaiTahapan').val(data.tglSelesai)
      
        $('#statusPelaksana').val('')

      date2 = $('#tglMulaiPelaksana').val()


    });

    $('#table_master tbody').on('click','#btnViewDataLangkah', function(){
      $('viewkodeInisial').val('')
      $('viewtglMulaiTahapan').val('')
      $('viewtglSelesaiTahapan').val('')
      $('viewtglMulaiPelaksana').val('')
      $('viewstatusPelaksana').val('')
      $('viewrangeData').val('')
      $('viewaktivitasLangkah').val('')

      var data = table.row( $(this).parents('tr') ).data();
      let kodeInisial = data.kodeInisial;
      $.ajax({
        url: '/kasubid/getPelaksanaView/'+kodeInisial,
        type: 'get',
        success:function(data)
        {
          data = data.data;
          console.log(data);
          $('#viewkodeInisial').val(data.kode_i);
          $('#viewtglMulaiTahapan').val(data.tgl_mulai);
          $('#viewtglSelesaiTahapan').val(data.tgl_selesai);
          $('#viewtglMulaiPelaksana').val(data.tgl_pelaksanaan);
          $('#viewstatusPelaksana').val(data.keterangan);
          $('#viewrangeData').val(data.persentase);
          $('#viewaktivitasLangkah').val(data.output);
        }

      })


    });

    $('#table_master tbody').on('click','#cetakPelaksana', function(){
      var data = table.row( $(this).parents('tr') ).data();
        let kodeInisial = data.kodeInisial;
        let url = "/kasubid/cetakPelaksana/"+ kodeInisial;
        window.location = url;
    });

    $('#tglMulaiPelaksana').on('focusout', function(){
      date1 = $('#tglSelesaiTahapan').val()
      date2 = $('#tglMulaiPelaksana').val()
    })

    $('#rangeData').on('change', function(){
      data = $('#rangeData').val()
      alert(data)
    })


  });
</script>

@endsection