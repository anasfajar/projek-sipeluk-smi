@extends('layouts.kasubid.master')

@section('title','Dashboard Kasubid')
<style>
.modal-add {
    max-width:70% !important;
    margin:0 auto;
    /*add what you want here*/
}
</style>

@section('content')

{{-- Start Right Side --}}

<div id="right-panel" class="right-panel">

    @include('include.kasubid.header_master')

    

      <div class="breadcrumbs">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Dashboard</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <div class="content mt-1">
        @if (session('status'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
  
        <div class="col-lg-12 col-sm-12">
          <div class="card">
              <div class="card-body">
                  <div class="stat-widget-one">
                      <div class="stat-icon dib"><i class="ti-book text-success border-success"></i></div>
                      <div class="stat-content dib">
                          <div class="stat-text">Total Tanggung Jawab Kegiatan</div>
                          <div class="stat-digit">{{$getCount?$getCount:'0'}}</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-dark">
                    <strong class="card-title" style="color:white">Table Create Detil Langkah Tahapan</strong>
                    {{-- <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#myModal" id="add_program"></button> --}}
                </div>
                <div class="card-body">
                    <table class="table table-striped"  id="table_master" >
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode </th>
                                <th scope="col">Tahapan Kegiatan</th>
                                <th scope="col">Tanggal Mulai</th>
                                <th scope="col">Tanggal Selesai</th>
                                <th scope="col">Durasi</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>

      </div>

      {{-- Tambah Anak Tahapan Kegiatan  --}}
      <div id="tambahTahapan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Jadwal 2</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="form-row">
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Kode Program</label>
                  <input type="text" class="form-control" id="kodeInisial" placeholder="Kode" readonly>
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Kode Langkah Tahapan</label>
                  <input type="text" class="form-control" id="kodeDetilLangkah" placeholder="" readonly>
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                  <label for="">Tanggal Mulai Kegiatan</label>
                  <input type="date" class="form-control" id="tglMulaiProgram" placeholder="Kode" readonly>
                </div>
                <div class="form-group col-lg-3 col-sm-12">
                <label for="">Tanggal Selesai Kegiatan</label>
                  <input type="date" class="form-control" id="tglSelesaiProgram" placeholder="" readonly>
                </div>
                {{-- <div class="form-group col-lg-8 col-sm-12">
                  <label for="">Program Kegiatan</label>
                  <input type="text" class="form-control" id="aktivitas_k_edit" placeholder="Program Kegiatan">
                </div> --}}
              </div>
              <div class="form-row">
                <div class="form-group col-lg-5 col-sm-12">
                  <label for="">Tanggal Mulai Langkah</label>
                  <input type="date" class="form-control" id="tglMulaiLangkah" placeholder="Kode" >
                </div>
                <div class="form-group col-lg-5 col-sm-12">
                <label for="">Tanggal Selesai Langkah</label>
                  <input type="date" class="form-control" id="tglSelesaiLangkah" placeholder="" >
                </div>
                <div class="form-group col-lg-2 col-sm-12">
                  <label for="">Durasi</label>
                  <input type="text" class="form-control" id="durasiLangkah" placeholder="Durasi" readonly>
                </div>
              </div>
              
              <div class="form-row">
                <div class="form-group col-lg-12 col-sm-12">
                  <label for="">Aktivitas Langkah</label>
                  <textarea type="text" class="form-control" rows="5" id="aktivitasLangkah" placeholder="Aktivitas Langkah" ></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" id="submitDataLangkah">Submit</button>
            </div>
          </div>

        </div>
      </div>



  {{-- ========================================== Modal Tambah Master Schedjule ================================================= --}}


      
</div>
<script>
  $(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    function diffDays(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
    
    table = $('#table_master').DataTable({
      
      ajax: {
          url: '/kasubid/table_data_kasubid',
          type:'get',
      },
      "columns" : [
                { "data" : "no" },
                { "data" : "kodeInisial" },
                { "data" : "aktivitas" },
                { "data" : "tglMulai" },
                { "data" : "tglSelesai" },
                { "data" : "durasiHari" },
            ],
        "columnDefs": [
          {
            "targets": 6,
            data: 'tipeKode',
            render:function(data,type,row,meta){
                return '<button class="btn btn-primary" data-toggle="modal" data-target="#tambahTahapan" id="tambahDetilTahapan"><span class="ti-plus"></span></button> ';
            }
          },
        ]      
    });
    
    kodeInisial = $('#kodeInisial').val('');
    kodeDetilLangkah = $('#kodeDetilLangkah').val('');
    tglMulaiLangkah = $('#tglMulaiLangkah').val('')
    tglSelesaiLangkah = $('#tglSelesaiLangkah').val('')
    durasiLangkah = $('#durasiLangkah').val('')

    $('#table_master tbody').on('click','#tambahDetilTahapan', function(){
      var data = table.row( $(this).parents('tr') ).data();

      $('#kodeInisial').val(data.kodeInisial);
      $('#tglMulaiProgram').val(data.tglMulai);
      $('#tglSelesaiProgram').val(data.tglSelesai);
      // alert(data.kodeInisial)
      $.ajax({
        url : '/kasubid/get_count_tahapan/'+data.kodeInisial,
        type : 'get',
        success:function(data)
        {
          $('#kodeDetilLangkah').val('00'+data.data);
        }
      })
    });

    $('#tglMulaiLangkah,#tglSelesaiLangkah').on('focusout', function(){
      date1 = $('#tglMulaiLangkah').val()
      date2 = $('#tglSelesaiLangkah').val()
      
      dateK1 = $('#tglMulaiProgram').val();
      dateK2 = $('#tglSelesaiProgram').val();

      if(date1 < dateK1)
      {
        alert('Tanggal mulai Langkah, melebihi tanggal mulai program');
        $('#tglMulaiLangkah').val('')
      }
      if(date2 > dateK2)
      {
        alert('Tanggal selesai Langkah, melebihi tanggal selesai program');
        $('#tglSelesaiLangkah').val('')
      }
      else
      {
          date1 = $('#tglMulaiLangkah').val()
          date2 = $('#tglSelesaiLangkah').val()
          hasilDurasi = diffDays(date1,date2);
          console.log(hasilDurasi)

          durasiLangkah = hasilDurasi +1;    

          if(isNaN(durasiLangkah))
          {
            $('#durasiLangkah').val('0')
          }
          else
          {
            $('#durasiLangkah').val(durasiLangkah)            
          }

      }

    })

    $('#submitDataLangkah').on('click', function(){
      kodeInisial = $('#kodeInisial').val();
      kodeDetilLangkah = $('#kodeDetilLangkah').val();
      tglMulaiLangkah = $('#tglMulaiLangkah').val()
      tglSelesaiLangkah = $('#tglSelesaiLangkah').val()
      durasiLangkah = $('#durasiLangkah').val()
      aktivitasLangkah = $('#aktivitasLangkah').val()

      $.ajax({
        url : '/kasubid/postDetilLangkah',
        type : 'post',
        data : {kodeInisial:kodeInisial,kodeDetilLangkah:kodeDetilLangkah,tglMulaiLangkah:tglMulaiLangkah,tglSelesaiLangkah:tglSelesaiLangkah,aktivitasLangkah:aktivitasLangkah,durasiLangkah:durasiLangkah},
        success:function()
        {
          location.reload()
        }
      })

    })

  });
</script>

@endsection