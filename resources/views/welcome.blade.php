<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIPELUK 1.0.3</title>
		<link rel="stylesheet" href="/css/app.css">
		<style>
			body{
				background-color: #f8f8f8;
			}
			.text-front
			{
				margin: 20% 0;
				text-align: center;
			}

			.text-front h2
			{
				font-size: 58px;
				font-weight: bold;
				text-shadow: 0px 10px 24px -10px rgba(0,0,0,0.75);
			}
		</style>
</head>
<body>
	<div class="container-fluid p-0 m-0">
		<nav class="navbar navbar-expand-lg container-fluid navbar-dark" style="background-color:#45969b">
			<div class="collapse navbar-collapse container p-2" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> SIPELUK</a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				</ul>
				@if (Route::has('login'))
					<div class="top-right links">
						@auth
							@if(Auth::user()->role === 'admin')
								<a class="btn btn-primary " href="{{ route('admin.dashboard') }}">Dashboard</a>

							@elseif(Auth::user()->role === 'kabid')
								<a class="btn btn-primary " href="{{ route('kabid.dashboard') }}">Dashboard</a>

							@elseif(Auth::user()->role === 'kasubid')
              <a class="btn btn-primary " href="{{ route('kasubid.dashboard') }}">Dashboard</a>

							@endif
						@else
							{{--  <a href="{{ route('login') }}">Login</a>  --}}
							<a class="btn btn-warning btn-lg my-2 my-sm-0" href="{{ route('login') }}" >Masuk</a>
						@endauth
					</div>
				@endif
			</div>
		</nav>
		<div class="container">
				<div class="row">
					<div class="col-lg-12 align-self-center text-front">
						<h2>SIPELUK 1.0.3</h2>
					</div>
				</div>
			</div>
	</div>

<script src="/js/app.js"></script>
</body>
</html>